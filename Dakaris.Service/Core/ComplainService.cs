﻿using AutoMapper;
using Dakaris.Contracts.Services;
using Dakaris.Database.Context;
using Dakaris.Model.Domain;
using Dakaris.Model.Dto.Complain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Dakaris.Service.Core
{
    public class ComplainService : IComplainService
    {
        private readonly DakarisContext dakarisContext;
        private readonly IMapper mapper;

        public ComplainService(DakarisContext dakarisContext, IMapper mapper)
        {
            this.dakarisContext = dakarisContext;
            this.mapper = mapper;
        }
        public async Task<bool> CreateAsync(ComplainDto complaintDto)
        {
            try
            {
                var domainObject = mapper.Map<Complain>(complaintDto);
                domainObject.Reportee = null;
                domainObject.Reporter = null;
                var local = dakarisContext.Set<ViewedNurse>().Local.FirstOrDefault(entr => entr.ViewerId == complaintDto.ReporterId && entr.VieweeId == complaintDto.ReporteeId);
                if (local != null)
                    dakarisContext.Entry(local).State = EntityState.Detached;
                var createdPackage = await dakarisContext.Complains.AddAsync(domainObject);
                //DisplayStates(dakarisContext.ChangeTracker.Entries());
                await dakarisContext.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool Create(ComplainDto complaintDto)
        {
            try
            {
                var domainObject = mapper.Map<Complain>(complaintDto);
                domainObject.Reportee = null;
                DisplayStates(dakarisContext.ChangeTracker.Entries());
                var createdPackage = dakarisContext.Complains.Add(domainObject);
                
                dakarisContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<List<ComplainDto>> GetAllAsync()
        {
            List<ComplainDto> selectedObjectList = new List<ComplainDto>();
            try
            {
                var domainObject = await dakarisContext.Complains
                    .Include(e=>e.Reportee)
                    .Include(e=>e.Reporter)
                    .ToListAsync();
                selectedObjectList = mapper.Map<List<ComplainDto>>(domainObject);

                return selectedObjectList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<ComplainDto> GetByIdAsync(long Id)
        {
            ComplainDto selectedObject = new ComplainDto();
            try
            {
                var domainObject = await dakarisContext.Complains.FirstAsync(s => s.Id == Id);
                selectedObject = mapper.Map<ComplainDto>(domainObject);

                return selectedObject;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<bool> DeactivateComplainAsync(long Id)
        {
            try
            {
                var domainObject = await dakarisContext.Complains.FirstAsync(s => s.Id == Id);
                if (domainObject != null)
                {
                    domainObject.IsActive = false;
                    var createdPackage = dakarisContext.Complains.Update(domainObject);
                    await dakarisContext.SaveChangesAsync();

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private static void DisplayStates(IEnumerable<EntityEntry> entries)
        {
            foreach (var entry in entries)
            {
                Console.WriteLine($"Entity: {entry.Entity.GetType().Name},State: { entry.State.ToString()}");
            }
        }

        public bool DeactivateComplain(long Id)
        {
            try
            {
                var domainObject = dakarisContext.Complains.First(s => s.Id == Id);
                if (domainObject != null)
                {
                    domainObject.IsActive = false;
                    var createdPackage = dakarisContext.Complains.Update(domainObject);
                    dakarisContext.SaveChanges();

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
