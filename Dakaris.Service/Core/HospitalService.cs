﻿using AutoMapper;
using Dakaris.Contracts.Services;
using Dakaris.Database.Context;
using Dakaris.Model.Dto.Hospital;
using Dakaris.Model.Enum;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dakaris.Service.Core
{
    public class HospitalService : IHospitalService
    {
        private readonly DakarisContext dakarisContext;
        private readonly IMapper mapper;

        public HospitalService(DakarisContext dakarisContext, IMapper mapper)
        {
            this.dakarisContext = dakarisContext;
            this.mapper = mapper;
        }

        public List<HospitalDto> GetAll()
        {
            List<HospitalDto> selectedObjectList = new List<HospitalDto>();
            try
            {
                var domainObject = dakarisContext.Hospitals.Where(h => h.IsActive).ToList();
                selectedObjectList = mapper.Map<List<HospitalDto>>(domainObject);

                return selectedObjectList;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<List<HospitalDto>> GetAllAsync()
        {
            List<HospitalDto> selectedObjectList = new List<HospitalDto>();
            try
            {
                var domainObject = await dakarisContext.Hospitals.ToListAsync();
                selectedObjectList = mapper.Map<List<HospitalDto>>(domainObject);

                return selectedObjectList;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<List<HospitalDto>> GetByDistrictAsync(string districtName)
        {
            List<HospitalDto> selectedObjects = new List<HospitalDto>();
            try
            {
                var domainObject = await dakarisContext.Hospitals.ToListAsync();
                domainObject = domainObject.Where(a => a.District.ToString().Equals(districtName)).ToList();
                selectedObjects = mapper.Map<List<HospitalDto>>(domainObject);

                return selectedObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<HospitalDto> GetByIdAsync(long Id)
        {
            HospitalDto selectedObject = new HospitalDto();
            try
            {
                var domainObject = await dakarisContext.Hospitals.FirstAsync(s => s.Id == Id);
                selectedObject = mapper.Map<HospitalDto>(domainObject);

                return selectedObject;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
