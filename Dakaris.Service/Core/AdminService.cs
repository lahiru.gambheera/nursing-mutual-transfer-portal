﻿using AutoMapper;
using Dakaris.Contracts.Services;
using Dakaris.Database.Context;
using Dakaris.Model.Domain;
using Dakaris.Model.Dto.Nurse;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dakaris.Service.Core
{
    public class AdminService : IAdminService
    {
        private readonly DakarisContext dakarisContext;
        private readonly IMapper mapper;

        public AdminService(DakarisContext dakarisContext, IMapper mapper)
        {
            this.dakarisContext = dakarisContext;
            this.mapper = mapper;
        }

        public AdminDto GetByEmail(string email)
        {
            AdminDto selectedAdmin = new AdminDto();
            try
            {
                var admin = dakarisContext.Admins.First(s => s.Email == email);

                if (admin != null)
                {
                    selectedAdmin = mapper.Map<AdminDto>(admin);
                    return selectedAdmin;
                }
                else
                {
                    return selectedAdmin;
                }
            }
            catch (Exception ex)
            {

                return selectedAdmin;
            }
        }

        public async Task<AdminDto> GetByEmailAsync(string email)
        {
            AdminDto selectedAdmin = new AdminDto();
            try
            {
                var admin = await dakarisContext.Admins.FirstAsync(s => s.Email == email);

                if (admin != null)
                {
                    selectedAdmin = mapper.Map<AdminDto>(admin);
                    return selectedAdmin;
                }
                else
                {
                    return await Task.FromResult(selectedAdmin);
                }
            }
            catch (Exception ex)
            {

                return selectedAdmin;
            }
        }
    }
}
