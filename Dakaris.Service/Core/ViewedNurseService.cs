﻿using AutoMapper;
using Dakaris.Contracts.Services;
using Dakaris.Database.Context;
using Dakaris.Model.Domain;
using Dakaris.Model.Dto.ViewedNurse;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dakaris.Service.Core
{
    public class ViewedNurseService : IViewedNurseService
    {
        private readonly DakarisContext dakarisContext;
        private readonly IMapper mapper;

        public ViewedNurseService(DakarisContext dakarisContext, IMapper mapper)
        {
            this.dakarisContext = dakarisContext;
            this.mapper = mapper;
        }

        public async Task<bool> CreateAsync(ViewedNurseDto viewedNurseDto)
        {
            try
            {
                var domainObject = mapper.Map<ViewedNurse>(viewedNurseDto);
                var createdPackage = await dakarisContext.ViewedNurses.AddAsync(domainObject);
                await dakarisContext.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> CreateAsync(long ViewerId, long VieweeId)
        {
            try
            {
                ViewedNurse viewed = new ViewedNurse();
                viewed.Id = 0;
                viewed.ViewerId = ViewerId;
                viewed.VieweeId = VieweeId;

                await dakarisContext.ViewedNurses.AddAsync(viewed);
                await dakarisContext.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Create(ViewedNurseDto viewedNurseDto)
        {
            try
            {
                var domainObject = mapper.Map<ViewedNurse>(viewedNurseDto);
                var createdPackage = dakarisContext.ViewedNurses.Add(domainObject);
                dakarisContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Create(long ViewerId, long VieweeId)
        {
            try
            {
                ViewedNurse viewed = new ViewedNurse();
                viewed.Id = 0;
                viewed.ViewerId = ViewerId;
                viewed.VieweeId = VieweeId;

                dakarisContext.ViewedNurses.Add(viewed);
                dakarisContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<ViewedNurseDto> GetByViwerAndVieweeIdAsync(long ViewerId, long VieweeId)
        {
            ViewedNurseDto selectedObject = new ViewedNurseDto();
            try
            {
                var item = await dakarisContext.ViewedNurses.Include(a => a.Viewee).FirstAsync(s => s.ViewerId == ViewerId && s.VieweeId == VieweeId);

                if (item != null)
                {
                    selectedObject = mapper.Map<ViewedNurseDto>(item);
                    return selectedObject;
                }
                else
                {
                    return await Task.FromResult(selectedObject);
                }
            }
            catch (Exception ex)
            {

                return selectedObject;
            }
        }

        public async Task<List<ViewedNurseDto>> GetByViewerIdAsync(long ViewerId)
        {
            List<ViewedNurseDto> selectedObject = new List<ViewedNurseDto>();
            try
            {
                var item = await dakarisContext.ViewedNurses.Include(a => a.Viewee).ToListAsync();

                if (item != null)
                {
                    selectedObject = mapper.Map<List<ViewedNurseDto>>(item);
                    selectedObject = selectedObject.Where(a => a.ViewerId == ViewerId).ToList();
                    return selectedObject;
                }
                else
                {
                    return await Task.FromResult(selectedObject);
                }
            }
            catch (Exception ex)
            {

                return selectedObject;
            }
        }

        public List<ViewedNurseDto> GetByViewerId(long ViewerId)
        {
            List<ViewedNurseDto> selectedObject = new List<ViewedNurseDto>();
            try
            {
                var item = dakarisContext.ViewedNurses.Include(a => a.Viewee).ToList();

                if (item != null)
                {
                    selectedObject = mapper.Map<List<ViewedNurseDto>>(item);
                    selectedObject = selectedObject.Where(a => a.ViewerId == ViewerId).ToList();
                    return selectedObject;
                }
                else
                {
                    return selectedObject;
                }
            }
            catch (Exception ex)
            {

                return selectedObject;
            }
        }
    }
}
