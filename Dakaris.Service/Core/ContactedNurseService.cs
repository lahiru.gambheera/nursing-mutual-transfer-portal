﻿using AutoMapper;
using Dakaris.Contracts.Services;
using Dakaris.Database.Context;
using Dakaris.Model.Domain;
using Dakaris.Model.Dto.ContactedNurse;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dakaris.Service.Core
{
    public class ContactedNurseService : IContactedNurseService
    {
        private readonly DakarisContext dakarisContext;
        private readonly IMapper mapper;

        public ContactedNurseService(DakarisContext dakarisContext, IMapper mapper)
        {
            this.dakarisContext = dakarisContext;
            this.mapper = mapper;
        }

        public bool Create(ContactedNurseDto contactedNurseDto)
        {
            try
            {
                var domainObject = mapper.Map<ContactedNurse>(contactedNurseDto);
                var createdPackage = dakarisContext.ContactedNurses.Add(domainObject);
                dakarisContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> CreateAsync(ContactedNurseDto contactedNurseDto)
        {
            try
            {
                var domainObject = mapper.Map<ContactedNurse>(contactedNurseDto);
                var createdPackage = await dakarisContext.ContactedNurses.AddAsync(domainObject);
                await dakarisContext.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool CreateWithViewNurse(long ContacterId, long ContacteeId)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> CreateWithViewNurseAsync(long ContacterId, long ContacteeId)
        {
            ContactedNurse contactedNurse = new ContactedNurse();
            contactedNurse.Id = 0;
            contactedNurse.ContacterId = ContacterId;
            contactedNurse.ContacteeId = ContacteeId;

            using (var transaction = dakarisContext.Database.BeginTransaction())
            {
                try
                {
                    var viewedNurse = dakarisContext.ViewedNurses.FirstOrDefault(a => a.ViewerId == ContacterId && a.VieweeId == ContacteeId);
                    if (viewedNurse == null)
                    {
                        ViewedNurse viewed = new ViewedNurse();
                        viewed.Id = 0;
                        viewed.ViewerId = ContacterId;
                        viewed.VieweeId = ContacteeId;

                        await dakarisContext.ViewedNurses.AddAsync(viewed);
                        await dakarisContext.SaveChangesAsync();
                    }
                    await dakarisContext.ContactedNurses.AddAsync(contactedNurse);
                    await dakarisContext.SaveChangesAsync();
                    transaction.Commit();

                    return true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return false;
                }
            }
                
        }

        public async Task<ContactedNurseDto> GetByContacterAndContacteeIdAsync(long ContacterId, long ContacteeId)
        {
            ContactedNurseDto selectedObject = new ContactedNurseDto();
            try
            {
                var item = await dakarisContext.ContactedNurses.Include(a=>a.Contactee).FirstAsync(s => s.ContacterId == ContacterId && s.ContacteeId == ContacteeId);

                if (item != null)
                {
                    selectedObject = mapper.Map<ContactedNurseDto>(item);
                    return selectedObject;
                }
                else
                {
                    return await Task.FromResult(selectedObject);
                }
            }
            catch (Exception ex)
            {

                return selectedObject;
            }
        }

        public async Task<List<ContactedNurseDto>> GetByContacterIdAsync(long ContacterId)
        {
            List<ContactedNurseDto> selectedObject = new List<ContactedNurseDto>();
            try
            {
                var item = await dakarisContext.ContactedNurses.Include(a => a.Contactee).ToListAsync();

                if (item != null)
                {
                    selectedObject = mapper.Map<List<ContactedNurseDto>>(item);
                    selectedObject = selectedObject.Where(a => a.ContacterId == ContacterId).ToList();
                    return selectedObject;
                }
                else
                {
                    return await Task.FromResult(selectedObject);
                }
            }
            catch (Exception ex)
            {

                return selectedObject;
            }
        }
    }
}
