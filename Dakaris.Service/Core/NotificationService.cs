﻿using AutoMapper;
using AutoMapper.Configuration;
using Dakaris.Contracts.Services;
using Dakaris.Database.Context;
using Dakaris.Model.Dto.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dakaris.Service.Core
{
    public class NotificationService : INotificationService
    {
        private readonly DakarisContext dakarisContext;
        private readonly IMapper mapper;

        public NotificationService(DakarisContext dakarisContext, IMapper mapper)
        {
            this.dakarisContext = dakarisContext;
            this.mapper = mapper;
        }

        public bool DeleteNotification(long notificationId)
        {
            try
            {
                var notification = dakarisContext.Notifications.FirstOrDefault(n => n.Id == notificationId);
                notification.IsDeleted = true;

                dakarisContext.Notifications.Attach(notification).Property(p => p.IsDeleted).IsModified = true;

                dakarisContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public List<NotificationDto> GetMyNotificationList(long nurseId, long page, long pageSize)
        {
            try
            {
                var notificationList = dakarisContext.Notifications
                    .OrderByDescending(n => n.Id)
                    .Where(n => n.NurseId == nurseId && !n.IsDeleted)
                    .Skip((int)((page-1)*pageSize))
                    .Take((int)pageSize)
                    .ToList();

                var notificationDtoList = mapper.Map<List<NotificationDto>>(notificationList);

                return notificationDtoList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
