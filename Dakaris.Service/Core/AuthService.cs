﻿using Dakaris.Contracts.Services;
using Dakaris.Model.Dto.User;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Google.Apis.Auth;
using Google.Apis.Auth.OAuth2;

namespace Dakaris.Service.Core
{
    public class AuthService : IAuthService
    {
        public async Task<bool> VerifyGoogleTokenAsync(GoogleUser googleUser)
        {
            try
            {
                var payload = await GoogleJsonWebSignature.ValidateAsync(googleUser.Token);

                if (payload.ExpirationTimeSeconds > 0) { return true; }

                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
