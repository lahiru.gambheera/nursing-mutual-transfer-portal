﻿using AutoMapper;
using Dakaris.Contracts.Services;
using Dakaris.Database.Context;
using Dakaris.Model.Domain;
using Dakaris.Model.Dto.Package;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dakaris.Service.Core
{
    public class PackageService : IPackageService
    {
        private readonly DakarisContext dakarisContext;
        private readonly IMapper mapper;

        public PackageService(DakarisContext dakarisContext, IMapper mapper)
        {
            this.dakarisContext = dakarisContext;
            this.mapper = mapper;
        }

        #region ACADAMIC PHASE

        public async Task<bool> CreateAsync(PackageDto packageDto)
        {
            try
            {
                var domainObject = mapper.Map<Package>(packageDto);
                var createdPackage = await dakarisContext.Packages.AddAsync(domainObject);
                await dakarisContext.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> DeactivatePackageAsync(long Id)
        {
            try
            {
                var domainObject = await dakarisContext.Packages.FirstAsync(s => s.Id == Id);
                if(domainObject != null)
                {
                    domainObject.IsActive = false;
                    var createdPackage = dakarisContext.Packages.Update(domainObject);
                    await dakarisContext.SaveChangesAsync();

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<PackageDto> GetByIdAsync(long Id)
        {
            PackageDto selectedObject = new PackageDto();
            try
            {
                var domainObject = await dakarisContext.Packages.FirstAsync(s => s.Id == Id);
                selectedObject = mapper.Map<PackageDto>(domainObject);

                return selectedObject;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<PackageDto> GetByNameAsync(string name)
        {
            PackageDto selectedObject = new PackageDto();
            try
            {
                var domainObject = await dakarisContext.Packages.FirstAsync(s => s.Name == name);
                selectedObject = mapper.Map<PackageDto>(domainObject);

                return selectedObject;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<bool> UpdateAsync(PackageDto packageDto)
        {
            Package selectedObject = new Package();
            try
            {
                selectedObject = mapper.Map<Package>(packageDto);
                var local = dakarisContext.Set<Package>().Local.FirstOrDefault(entry => entry.Id.Equals(selectedObject.Id));
                if (local != null)
                    dakarisContext.Entry(local).State = EntityState.Detached;
                var createdPackage = dakarisContext.Packages.Update(selectedObject).State = EntityState.Modified;
                await dakarisContext.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> DeleteAsync(PackageDto packageDto)
        {
            Package selectedObject = new Package();
            try
            {
                selectedObject = mapper.Map<Package>(packageDto);
                var local = dakarisContext.Set<Package>().Local.FirstOrDefault(entry => entry.Id.Equals(selectedObject.Id));
                if (local != null)
                    dakarisContext.Entry(local).State = EntityState.Detached;
                var createdPackage = dakarisContext.Packages.Remove(selectedObject);
                await dakarisContext.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Create(PackageDto packageDto)
        {
            try
            {
                var domainObject = mapper.Map<Package>(packageDto);
                var createdPackage = dakarisContext.Packages.Add(domainObject);
                dakarisContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Update(PackageDto packageDto)
        {
            Package selectedObject = new Package();
            try
            {
                selectedObject = mapper.Map<Package>(packageDto);
                var local = dakarisContext.Set<Package>().Local.FirstOrDefault(entry => entry.Id.Equals(selectedObject.Id));
                if (local != null)
                    dakarisContext.Entry(local).State = EntityState.Detached;
                var createdPackage = dakarisContext.Packages.Update(selectedObject).State = EntityState.Modified;
                dakarisContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #endregion

        public async Task<List<PackageDto>> GetAllAsync()
        {
            List<PackageDto> selectedObjectList = new List<PackageDto>();
            try
            {
                var domainObject = await dakarisContext.Packages.Where(p => p.IsActive).ToListAsync();
                selectedObjectList = mapper.Map<List<PackageDto>>(domainObject);

                return selectedObjectList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<bool> RequestForAPackageAsync(long nurseId, long packageId)
        {
            try
            {
                var packageRequest = new PackageRequest();
                packageRequest.NurseId = nurseId;
                packageRequest.PackageId = packageId;

                await dakarisContext.PackageRequests.AddAsync(packageRequest);
                int effectedRowCount = await dakarisContext.SaveChangesAsync();

                return effectedRowCount > 0;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
