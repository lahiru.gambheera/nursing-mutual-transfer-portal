﻿using AutoMapper;
using Dakaris.Contracts.Services;
using Dakaris.Database.Context;
using Dakaris.Model.Dto.Nurse;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dakaris.Service.Core
{
    public class NurseExpectingHospitalService : INurseExpectingHospitalService
    {
        private readonly DakarisContext dakarisContext;
        private readonly IMapper mapper;

        public NurseExpectingHospitalService(DakarisContext dakarisContext, IMapper mapper)
        {
            this.dakarisContext = dakarisContext;
            this.mapper = mapper;
        }

        public async Task<List<NurseExpectingHospitalDto>> GetAllAsync()
        {
            List<NurseExpectingHospitalDto> selectedObjectList = new List<NurseExpectingHospitalDto>();
            try
            {
                var domainObject = await dakarisContext.NurseExpectingHospitals.ToListAsync();
                selectedObjectList = mapper.Map<List<NurseExpectingHospitalDto>>(domainObject);

                return selectedObjectList;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<List<NurseExpectingHospitalDto>> GetByHospitalIdAsync(long hospitalId)
        {
            List<NurseExpectingHospitalDto> selectedObjects = new List<NurseExpectingHospitalDto>();
            try
            {
                var domainObject = await dakarisContext.NurseExpectingHospitals.ToListAsync();
                domainObject = domainObject.Where(a => a.hospitalId == hospitalId).ToList();
                selectedObjects = mapper.Map<List<NurseExpectingHospitalDto>>(domainObject);

                return selectedObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<NurseExpectingHospitalDto> GetByIdAsync(long Id)
        {
            NurseExpectingHospitalDto selectedObject = new NurseExpectingHospitalDto();
            try
            {
                var domainObject = await dakarisContext.NurseExpectingHospitals.FirstAsync(s => s.Id == Id);
                selectedObject = mapper.Map<NurseExpectingHospitalDto>(domainObject);

                return selectedObject;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<List<NurseExpectingHospitalDto>> GetByNurseIdAsync(long nurseId)
        {
            List<NurseExpectingHospitalDto> selectedObjects = new List<NurseExpectingHospitalDto>();
            try
            {
                var domainObject = await dakarisContext.NurseExpectingHospitals.ToListAsync();
                domainObject = domainObject.Where(a => a.NurseId == nurseId).ToList();
                selectedObjects = mapper.Map<List<NurseExpectingHospitalDto>>(domainObject);

                return selectedObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
