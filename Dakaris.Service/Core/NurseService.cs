﻿using AutoMapper;
using Dakaris.Contracts.Services;
using Dakaris.Database.Context;
using Dakaris.Model.Domain;
using Dakaris.Model.Dto.Nurse;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using LinqKit;
using Microsoft.Extensions.Configuration;
using Dakaris.Utility.Encriptors;
using Dakaris.Utility.Enums;
using Dakaris.Model.Enum;
using Dakaris.Model.Dto.Hospital;
using Dakaris.Model.Dto.User;

namespace Dakaris.Service.Core
{
    public class NurseService : INurseService
    {
        private readonly DakarisContext dakarisContext;
        private readonly IMapper mapper;
        private readonly IConfiguration configuration;

        public NurseService(DakarisContext dakarisContext, IConfiguration configuration, IMapper mapper)
        {
            this.dakarisContext = dakarisContext;
            this.configuration = configuration;
            this.mapper = mapper;
        }

        #region ACADAMIC PHASE

        public async Task<List<NurseDto>> GetAllAsync()
        {
            List<NurseDto> selectedObjectList = new List<NurseDto>();
            try
            {
                var domainObject = await dakarisContext.Nurses.ToListAsync();
                selectedObjectList = mapper.Map<List<NurseDto>>(domainObject);

                return selectedObjectList;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<NurseDto> GetAll()
        {
            List<NurseDto> selectedObjectList = new List<NurseDto>();
            try
            {
                var domainObject = dakarisContext.Nurses.ToList();
                selectedObjectList = mapper.Map<List<NurseDto>>(domainObject);

                return selectedObjectList;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<NurseDto> GetByEmailAsync(string email)
        {
            NurseDto selectedNurse = new NurseDto();
            try
            {
                var Nurse = await dakarisContext.Nurses.Include(a => a.Package).FirstAsync(s => s.Email == email);

                if (Nurse != null)
                {
                    selectedNurse = mapper.Map<NurseDto>(Nurse);
                    return selectedNurse;
                }
                else
                {
                    return await Task.FromResult(selectedNurse);
                }
            }
            catch (Exception ex)
            {

                return selectedNurse;
            }
        }

        public NurseDto GetByEmail(string email)
        {
            NurseDto selectedNurse = new NurseDto();
            try
            {
                var Nurse = dakarisContext.Nurses.Include(a => a.Package).First(s => s.Email == email);

                if (Nurse != null)
                {
                    selectedNurse = mapper.Map<NurseDto>(Nurse);
                    return selectedNurse;
                }
                else
                {
                    return selectedNurse;
                }
            }
            catch (Exception ex)
            {

                return selectedNurse;
            }
        }


        public NurseDto GetById(long Id)
        {
            NurseDto selectedNurse = new NurseDto();
            try
            {
                var Nurse = dakarisContext.Nurses.First(s => s.Id == Id);

                if (Nurse != null)
                {
                    selectedNurse = mapper.Map<NurseDto>(Nurse);
                    return selectedNurse;
                }
                else
                {
                    return selectedNurse;
                }
            }
            catch (Exception ex)
            {

                return selectedNurse;
            }
        }

        public async Task<bool> ActivateAcountAsync(long Id)
        {
            try
            {
                var domainObject = await dakarisContext.Nurses.FirstAsync(s => s.Id == Id);
                if (domainObject != null)
                {
                    domainObject.IsActive = true;
                    domainObject.IsSuspended = false;
                    var createdPackage = dakarisContext.Nurses.Update(domainObject);
                    await dakarisContext.SaveChangesAsync();

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<bool> SuspendAcountAsync(long Id)
        {
            try
            {
                var domainObject = await dakarisContext.Nurses.FirstAsync(s => s.Id == Id);
                if (domainObject != null)
                {
                    domainObject.IsActive = false;
                    domainObject.IsSuspended = true;
                    var createdPackage = dakarisContext.Nurses.Update(domainObject);
                    await dakarisContext.SaveChangesAsync();

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<List<NurseDto>> GetAllByCurrentAndExpectedHospitalAsync(long currentHospital, long expectedHospital)
        {
            List<NurseDto> selectedObjectList = new List<NurseDto>();
            try
            {
                var domainObject = await dakarisContext.Nurses
                    .Include(a => a.NurseExpectingHospitalList)
                    .Include(a => a.Package)
                    .Where(a => a.IsActive == true && a.IsSuspended == false &&
                    a.NurseExpectingHospitalList.Count > 0).ToListAsync();

                if (currentHospital == 0 && expectedHospital == 0)
                {
                    selectedObjectList = mapper.Map<List<NurseDto>>(domainObject);
                }
                else if (currentHospital > 0 && expectedHospital == 0)
                {
                    var selectedList = domainObject.Where(a => (a.NurseExpectingHospitalList.Any(x => x.hospitalId == currentHospital)));
                    selectedObjectList = mapper.Map<List<NurseDto>>(selectedList);
                }
                else if (currentHospital == 0 && expectedHospital > 0)
                {
                    var selectedList = domainObject.Where(a => a.CurrentHospitalId == expectedHospital);
                    selectedObjectList = mapper.Map<List<NurseDto>>(selectedList);
                }
                else
                {
                    var selectedList = domainObject.Where(a => a.CurrentHospitalId == expectedHospital && (a.NurseExpectingHospitalList.Any(x => x.hospitalId == currentHospital)));
                    selectedObjectList = mapper.Map<List<NurseDto>>(selectedList);
                }


                return selectedObjectList;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<List<NurseDto>> GetAllByCurrentAndExpectedHospitalAsync(long currentHospital, long[] expectedHospitals)
        {
            List<NurseDto> selectedObjectList = new List<NurseDto>();
            try
            {
                var domainObject = await dakarisContext.Nurses.Include(a => a.NurseExpectingHospitalList).ToListAsync();
                var selectedList = domainObject.Where(a => a.CurrentHospitalId == currentHospital && (a.NurseExpectingHospitalList.Any(x => expectedHospitals.Contains(x.hospitalId))));
                selectedObjectList = mapper.Map<List<NurseDto>>(selectedList);

                return selectedObjectList;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool Create(NurseDto nurseDto)
        {
            try
            {
                var domainObject = mapper.Map<Nurse>(nurseDto);
                var createdNurse = dakarisContext.Nurses.Add(domainObject);
                dakarisContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool Update(NurseDto nurseDto)
        {
            throw new NotImplementedException();
        }

        #endregion

        public async Task<List<NurseDto>> SearchAsync(long fromHospitalId, long toHospitalId, int page, int pageSize)
        {
            try
            {
                string defaultPageSize = configuration.GetValue<string>("DefaultValues:SearchNursePageSize");
                pageSize = int.Parse(defaultPageSize);

                List<NurseDto> selectedObjectList = new List<NurseDto>();

                Expression<Func<Nurse, bool>> predicate = PredicateBuilder.New<Nurse>(true);
                if (fromHospitalId > 0)
                {
                    predicate = predicate.And(p => p.CurrentHospitalId == fromHospitalId);
                }

                if (toHospitalId > 0)
                {
                    predicate = predicate.And(p => p.NurseExpectingHospitalList.Any(e => e.hospitalId == toHospitalId));
                }

                predicate = predicate.And(p => p.IsActive);

                var nurseList = await dakarisContext.Nurses
                                    .OrderByDescending(n => n.Id)
                                    .Where(predicate)
                                    .Skip(page * pageSize)
                                    .Take(pageSize)
                                    .ToListAsync();

                selectedObjectList = mapper.Map<List<NurseDto>>(nurseList);

                selectedObjectList.Select(n =>
                {
                    n.Name = (dakarisContext.GirlyNames.FirstOrDefault(g => g.Id == getGirlyNameId(n.Id))).Name;
                    n.AvatarUrl = (dakarisContext.GirlyNames.FirstOrDefault(g => g.Id == getGirlyNameId(n.Id))).AvatarUrl;
                    n.CurrentHospital = new HospitalDto();
                    n.CurrentHospital.Name = dakarisContext.Hospitals.FirstOrDefault(h => h.Id == n.CurrentHospitalId).Name;
                    return n;
                });

                selectedObjectList.ForEach(n => n.EncriptAllSensitiveData());

                return selectedObjectList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<List<NurseDto>> SearchAsync(int fromDistrictId, int toDistrictId, int page, int pageSize)
        {
            try
            {
                string defaultPageSize = configuration.GetValue<string>("DefaultValues:SearchNursePageSize");
                // pageSize = int.Parse(defaultPageSize);

                if (pageSize > int.Parse(defaultPageSize)) { return new List<NurseDto>(); }

                List<NurseDto> selectedObjectList = new List<NurseDto>();

                Expression<Func<Nurse, bool>> predicate = PredicateBuilder.New<Nurse>(true);
                if (fromDistrictId > 0)
                {
                    predicate = predicate.And(p => p.CurrentHospital.District == (District)fromDistrictId);
                }

                if (toDistrictId > 0)
                {
                    predicate = predicate.And(p => p.NurseExpectingHospitalList.Any(e => e.Hospital.District == (District)toDistrictId));
                }

                predicate = predicate.And(p => p.IsActive);

                var nurseList = await dakarisContext.Nurses
                    .Include(n => n.CurrentHospital)
                    .Include(n => n.NurseExpectingHospitalList)
                    .ThenInclude(ne => ne.Hospital)
                    .Where(n => (n.CurrentHospital.District == (District)fromDistrictId) &&
                                 n.NurseExpectingHospitalList.Any(h => h.Hospital.District == (District)toDistrictId))
                                    .Skip(page * pageSize)
                                    .Take(pageSize)
                    .OrderByDescending(n => n.Id)
                    .ToListAsync();

                selectedObjectList = mapper.Map<List<NurseDto>>(nurseList);

                selectedObjectList.ForEach(n => n.EncriptAllSensitiveData());

                return selectedObjectList;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<bool> CreateAsync(NurseRegisterDto nurseDto)
        {
            try
            {
                var nurse = mapper.Map<Nurse>(nurseDto);
                nurse.IsActive = true;
                nurse.IsSuspended = false;
                nurse.HasPaidForPromo = false;
                nurse.PackageId = 1;
                var createdNurse = await dakarisContext.Nurses.AddAsync(nurse);
                await dakarisContext.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<bool> UpdateAsync(NurseDto nurseDto)
        {
            try
            {
                nurseDto.DecryptId();

                var domainObject = mapper.Map<Nurse>(nurseDto);

                dakarisContext.Nurses.Attach(domainObject).Property(p => p.Name).IsModified = true;
                dakarisContext.Nurses.Attach(domainObject).Property(p => p.MobileNumber).IsModified = true;
                dakarisContext.Nurses.Attach(domainObject).Property(p => p.ExtraMobileNo).IsModified = true;

                await dakarisContext.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<NurseDto> GetByIdAsync(string encriptedId)
        {

            try
            {
                long id = Encriptor.DecryptToLong(encriptedId, EncriptObjectType.Nurse);
                NurseDto selectedNurse = new NurseDto();
                var Nurse = await dakarisContext.Nurses.FirstAsync(s => s.Id == id);

                if (Nurse == null) { return selectedNurse; }

                selectedNurse = mapper.Map<NurseDto>(Nurse);
                return selectedNurse;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<NurseDto> GetMyDataAsync(long id)
        {
            try
            {
                NurseDto selectedNurse = new NurseDto();
                var Nurse = await dakarisContext.Nurses
                    .Include(n => n.CurrentHospital)
                    .Include(n => n.NurseExpectingHospitalList)
                    .ThenInclude(ne => ne.Hospital)
                    .Include(ne => ne.Package)
                    .FirstAsync(s => s.Id == id);

                if (Nurse == null) { return selectedNurse; }

                selectedNurse = mapper.Map<NurseDto>(Nurse);
                selectedNurse.EncryptId();
                return selectedNurse;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<List<PreviouslyContactedNurse>> GetMyContactedNurseListAsync(long nurseId)
        {
            try
            {
                var contactedNurseList = await dakarisContext.ContactedNurses
                    .Where(cn => cn.ContacterId == nurseId)
                    .Include(n => n.Contactee)
                    .ToListAsync();

                var resultList = new List<PreviouslyContactedNurse>();

                contactedNurseList.ForEach(nurse =>
                {
                    var contactee = nurse.Contactee;

                    resultList.Add(new PreviouslyContactedNurse()
                    {
                        Name = contactee.Name,
                        CurrentHospital = contactee.CurrentHospital.Name + " - " + contactee.CurrentHospital.District,
                        Phone1 = contactee.MobileNumber,
                        Phone2 = contactee.ExtraMobileNo,
                        EncriptedId = Encriptor.EncryptFromLong(contactee.Id, EncriptObjectType.Nurse),
                        ExpectedHospitalList = contactee.NurseExpectingHospitalList.ConvertAll(eh => eh.Hospital.Name + " - " + eh.Hospital.District)
                    });
                });

                return resultList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<UnlockingContact> UnlockFullContact(long contacterId, string encriptedContacteeId)
        {
            try
            {
                long contacteeId = Encriptor.DecryptToLong(encriptedContacteeId, EncriptObjectType.Nurse);

                var existingContact = await dakarisContext
                        .ContactedNurses
                        .Where(n => n.ContacterId == contacterId && n.ContacteeId == contacteeId)
                        .Include(n => n.Contactee).ThenInclude(n => n.CurrentHospital)
                        .Include(n => n.Contactee).ThenInclude(n => n.NurseExpectingHospitalList)
                        .Include(n => n.Contactee).ThenInclude(n => n.NurseExpectingHospitalList)
                        .FirstOrDefaultAsync();

                var notificationState = NotificationSendingStage.JustView;

                var contacter = await dakarisContext.Nurses.Where(n => n.Id == contacterId).FirstOrDefaultAsync();

                // Increasing the view count
                contacter.ViewCount = contacter.ViewCount + 1;
                dakarisContext.Nurses.Attach(contacter).Property(p => p.ViewCount).IsModified = true;
                await dakarisContext.SaveChangesAsync();

                // Checking whether this contactee has contacted previously
                if (existingContact != null)
                {
                    // If contactee is previously contacted, send the full contact

                    var contactee = existingContact.Contactee;

                    return new UnlockingContact()
                    {
                        IsUnlocked = true,
                        Contactee = new PreviouslyContactedNurse()
                        {
                            Name = contactee.Name,
                            CurrentHospital = contactee.CurrentHospital.Name + " - " + contactee.CurrentHospital.District,
                            Phone1 = contactee.MobileNumber,
                            Phone2 = contactee.ExtraMobileNo,
                            EncriptedId = Encriptor.EncryptFromLong(contactee.Id, EncriptObjectType.Nurse),
                            ExpectedHospitalList = contactee.NurseExpectingHospitalList.ConvertAll(eh => eh.Hospital.Name + " - " + eh.Hospital.District)
                        }
                    };
                }


                // If contactee is not contacted before, This is time to send the contact.

                // Checking contacter's credit availability.
                if (contacter.AvailableViewCredits > 0)
                {
                    var contactee = await dakarisContext.Nurses.Where(n => n.Id == contacteeId).FirstOrDefaultAsync();

                    // Checking contactee's credit availability.
                    if (contactee.AvailableViewCredits > 0)
                    {
                        var transaction = await dakarisContext.Database.BeginTransactionAsync();

                        // Reducing contacter's credits
                        int creditsToBe = contacter.AvailableViewCredits - 1;
                        contacter.AvailableViewCredits = creditsToBe;
                        dakarisContext.Nurses.Attach(contacter).Property(p => p.AvailableViewCredits).IsModified = true;

                        // Downgrading the package
                        if (creditsToBe <= 0)
                        {
                            contacter.PackageId = (await dakarisContext.Packages.FirstOrDefaultAsync(p => p.Code == "L0")).Id;
                            dakarisContext.Nurses.Attach(contacter).Property(p => p.PackageId).IsModified = true;

                            var currentPackage = await dakarisContext.PackageRequests.FirstOrDefaultAsync(pr => pr.NurseId == contacterId && pr.Status == Model.Enum.NursePackageStatus.Active);
                            currentPackage.Status = NursePackageStatus.CreditLimitExeeded;
                            dakarisContext.PackageRequests.Attach(currentPackage).Property(p => p.Status).IsModified = true;
                        }

                        await dakarisContext.SaveChangesAsync();


                        if (existingContact == null)
                        {
                            // Recording the contacted history
                            var newContactedNurse = new ContactedNurse()
                            {
                                ContacteeId = contactee.Id,
                                ContacterId = contacter.Id
                            };

                            await dakarisContext.ContactedNurses.AddAsync(newContactedNurse);
                            await dakarisContext.SaveChangesAsync();
                        }

                        await transaction.CommitAsync();

                        // Notify about this to contactee
                        notificationState = NotificationSendingStage.PickedContact;
                        SendNotification(contacterId, contacteeId, notificationState);

                        // Everything is fine. Time to send the contact
                        return new UnlockingContact()
                        {
                            IsUnlocked = true,
                            Contactee = new PreviouslyContactedNurse()
                            {
                                Name = contactee.Name,
                                CurrentHospital = contactee.CurrentHospital.Name + " - " + contactee.CurrentHospital.District,
                                Phone1 = contactee.MobileNumber,
                                Phone2 = contactee.ExtraMobileNo,
                                EncriptedId = Encriptor.EncryptFromLong(contactee.Id, EncriptObjectType.Nurse),
                                ExpectedHospitalList = contactee.NurseExpectingHospitalList.ConvertAll(eh => eh.Hospital.Name + " - " + eh.Hospital.District)
                            }
                        };
                    }

                    notificationState = NotificationSendingStage.UpgradeToPickContact;
                    SendNotification(contacterId, contacteeId, notificationState);
                }

                // Send a notification
                SendNotification(contacterId, contacteeId, notificationState);

                return new UnlockingContact()
                {
                    IsUnlocked = false,
                    Message = "You have no available view credits. Please upgrade your plan"
                };
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<RegistrationInfoDto> isRegisteredAsync(GoogleUser googleUser)
        {
            try
            {
                var availableUser = await dakarisContext.Nurses.FirstOrDefaultAsync(n => n.Email == googleUser.Email);

                if (availableUser == null)
                {
                    var nurse = new Nurse();
                    nurse.Email = googleUser.Email;
                    nurse.Status = NurseStatus.just_logged_in;
                    nurse.Name = googleUser.Name;
                    nurse.GoogleId = googleUser.UserId;
                    nurse.PackageId = 1;

                    var newNurse = await dakarisContext.Nurses.AddAsync(nurse);
                    await dakarisContext.SaveChangesAsync();

                    return new RegistrationInfoDto()
                    {
                        Id = newNurse.Entity.Id,
                        IsRegistered = false,
                        IsValidUser = true
                    };
                }

                return new RegistrationInfoDto()
                {
                    Id = availableUser.Id,
                    IsRegistered = true,
                    IsValidUser = !availableUser.IsSuspended
                };
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private async Task<bool> SendNotification(long contacterId, long contacteeId, NotificationSendingStage notificationSendingStage)
        {
            string message = "Someone has tried to look your profile. Keep in-touch. It may be your mutual partner :).";
            bool isActionbuttonAvailable = false;
            string actionButtonText = "";
            string actionButtonLink = "";

            if (notificationSendingStage == NotificationSendingStage.PickedContact)
            {
                message = "Someone has unlocked your contact. You will get a call from that nurse. Keep your phone ready";
            }
            else if (notificationSendingStage == NotificationSendingStage.UpgradeToPickContact)
            {
                message = "Someone tried to unlock your contact. But since you are not upgraded your package, she couldn't contact you. Please upgrade to contact a mutual partner";
                isActionbuttonAvailable = true;
                actionButtonText = "View Packages";
                actionButtonLink = "/packages";

            }

            var notification = new Notification()
            {
                NurseId = contacterId,
                HiddenMessage = "ViewerId: " + contacteeId,
                Message = message,
                IsActionButtonAvailable = isActionbuttonAvailable,
                ActionButtonText = actionButtonText,
                ActionButtonLink = actionButtonLink,
                Time = DateTime.UtcNow
            };
            await dakarisContext.Notifications.AddAsync(notification);
            await dakarisContext.SaveChangesAsync();

            return true;
        }

        private int getGirlyNameId(long nurseId)
        {
            return (int)nurseId % 100;
        }


    }
    enum NotificationSendingStage
    {
        JustView = 1,
        PickedContact = 2,
        UpgradeToPickContact = 3
    }
}
