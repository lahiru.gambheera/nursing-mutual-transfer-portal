﻿using Dakaris.Model.Dto.Nurse;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Dakaris.Contracts.Services
{
    public interface IAdminService
    {
        Task<AdminDto> GetByEmailAsync(string email);
        AdminDto GetByEmail(string email);
    }
}
