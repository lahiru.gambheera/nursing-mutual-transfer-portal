﻿using Dakaris.Model.Dto.Notification;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dakaris.Contracts.Services
{
    public interface INotificationService
    {
        List<NotificationDto> GetMyNotificationList(long nurseId, long page, long pageSize);
        bool DeleteNotification(long notificationId);
    }
}
