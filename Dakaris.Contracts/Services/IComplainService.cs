﻿using Dakaris.Model.Dto.Complain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Dakaris.Contracts.Services
{
    public interface IComplainService
    {
        Task<bool> CreateAsync(ComplainDto complaintDto);
        bool Create(ComplainDto complaintDto);
        Task<ComplainDto> GetByIdAsync(long Id);
        Task<List<ComplainDto>> GetAllAsync();
        Task<bool> DeactivateComplainAsync(long Id);
        bool DeactivateComplain(long Id);
    }
}
