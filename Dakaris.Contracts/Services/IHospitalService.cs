﻿using Dakaris.Model.Dto.Hospital;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Dakaris.Contracts.Services
{
    public interface IHospitalService
    {
        Task<HospitalDto> GetByIdAsync(long Id);
        Task<List<HospitalDto>> GetAllAsync();
        List<HospitalDto> GetAll();
        Task<List<HospitalDto>> GetByDistrictAsync(string districtName);
    }
}
