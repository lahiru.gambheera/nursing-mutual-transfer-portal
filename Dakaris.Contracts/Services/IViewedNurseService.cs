﻿using Dakaris.Model.Dto.ViewedNurse;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Dakaris.Contracts.Services
{
    public interface IViewedNurseService
    {
        Task<bool> CreateAsync(ViewedNurseDto viewedNurseDto);
        bool Create(ViewedNurseDto viewedNurseDto);
        Task<bool> CreateAsync(long ViewerId, long VieweeId);
        bool Create(long ViewerId, long VieweeId);
        Task<ViewedNurseDto> GetByViwerAndVieweeIdAsync(long ViewerId, long VieweeId);
        Task<List<ViewedNurseDto>> GetByViewerIdAsync(long ViewerId);
        List<ViewedNurseDto> GetByViewerId(long ViewerId);
    }
}
