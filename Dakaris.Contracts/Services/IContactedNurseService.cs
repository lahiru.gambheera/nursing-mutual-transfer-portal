﻿using Dakaris.Model.Dto.ContactedNurse;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Dakaris.Contracts.Services
{
    public interface IContactedNurseService
    {
        Task<bool> CreateAsync(ContactedNurseDto contactedNurseDto);
        bool Create(ContactedNurseDto contactedNurseDto);
        Task<bool> CreateWithViewNurseAsync(long ContacterId, long ContacteeId);
        bool CreateWithViewNurse(long ContacterId, long ContacteeId);
        Task<ContactedNurseDto> GetByContacterAndContacteeIdAsync(long ContacterId, long ContacteeId);
        Task<List<ContactedNurseDto>> GetByContacterIdAsync(long ContacterId);
    }
}
