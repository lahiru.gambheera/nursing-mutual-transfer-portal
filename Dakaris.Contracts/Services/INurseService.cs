﻿using Dakaris.Model.Dto.Nurse;
using Dakaris.Model.Dto.User;
using Dakaris.Model.Enum;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Dakaris.Contracts.Services
{
    public interface INurseService
    {
        Task<bool> CreateAsync(NurseRegisterDto nurseDto);
        bool Create(NurseDto nurseDto);
        Task<bool> UpdateAsync(NurseDto nurseDto);
        bool Update(NurseDto nurseDto);
        Task<NurseDto> GetMyDataAsync(long id);
        Task<NurseDto> GetByIdAsync(string encriptedId);
        NurseDto GetById(long Id);
        Task<List<NurseDto>> SearchAsync(long fromHospitalId, long toHospitalId, int page, int pageSize);
        Task<List<NurseDto>> SearchAsync(int fromDistrictId, int toDistrictId, int page, int pageSize);
        Task<bool> ActivateAcountAsync(long Id);
        Task<bool> SuspendAcountAsync(long Id);
        Task<NurseDto> GetByEmailAsync(string email);
        NurseDto GetByEmail(string email);
        Task<List<NurseDto>> GetAllAsync();
        List<NurseDto> GetAll();
        Task<List<NurseDto>> GetAllByCurrentAndExpectedHospitalAsync(long currentHospital, long expectedHospital);
        Task<List<NurseDto>> GetAllByCurrentAndExpectedHospitalAsync(long currentHospital, long[] expectedHospitals);
        Task<List<PreviouslyContactedNurse>> GetMyContactedNurseListAsync(long contacterId);
        Task<UnlockingContact> UnlockFullContact(long contacterId, string encriptedContacteeId);
        Task<RegistrationInfoDto> isRegisteredAsync(GoogleUser googleUser);


    }
}
