﻿using Dakaris.Model.Dto.User;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Dakaris.Contracts.Services
{
    public interface IAuthService
    {
        public Task<bool> VerifyGoogleTokenAsync(GoogleUser googleUser);
    }
}
