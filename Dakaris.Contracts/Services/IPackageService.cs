﻿using Dakaris.Model.Dto.Package;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Dakaris.Contracts.Services
{
    public interface IPackageService
    {
        Task<bool> CreateAsync(PackageDto packageDto);
        bool Create(PackageDto packageDto);
        Task<bool> UpdateAsync(PackageDto packageDto);
        bool Update(PackageDto packageDto);
        Task<PackageDto> GetByIdAsync(long Id);
        Task<List<PackageDto>> GetAllAsync();
        Task<bool> DeactivatePackageAsync(long Id);
        Task<PackageDto> GetByNameAsync(string name);
        Task<bool> DeleteAsync(PackageDto packageDto);
        Task<bool> RequestForAPackageAsync(long nurseId, long packageId);

    }
}
