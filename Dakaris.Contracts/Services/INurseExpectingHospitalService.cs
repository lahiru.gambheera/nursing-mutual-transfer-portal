﻿using Dakaris.Model.Dto.Nurse;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Dakaris.Contracts.Services
{
    public interface INurseExpectingHospitalService
    {
        Task<NurseExpectingHospitalDto> GetByIdAsync(long Id);
        Task<List<NurseExpectingHospitalDto>> GetAllAsync();
        Task<List<NurseExpectingHospitalDto>> GetByNurseIdAsync(long nurseId);
        Task<List<NurseExpectingHospitalDto>> GetByHospitalIdAsync(long hospitalId);
    }
}
