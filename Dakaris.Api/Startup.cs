﻿using AutoMapper;
using Dakaris.Api.Mapping;
using Dakaris.Contracts.Services;
using Dakaris.Database.Context;
using Dakaris.Service.Core;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using System.Net;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
// using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace Dakaris.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }


        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
                        {
                            c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
                        });

            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));


            // Your connection string here...
            // var connection = @"Server=DESKTOP-8GD3GP9\SQLEXPRESS;Database=DakarisDb2;Trusted_Connection=True;ConnectRetryCount=0";

            var connection = @"Server=DESKTOP-8HN7I8B;Database=DakarisDb_2;Trusted_Connection=True;ConnectRetryCount=0";
            // var connection = Configuration.GetConnectionString("DakarisConnectionString");

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = false,
            ValidateAudience = false,
            ValidateLifetime = true,
            ValidateIssuerSigningKey = false,
            ValidIssuer = Configuration["Jwt:Issuer"],
            ValidAudience = Configuration["Jwt:Issuer"],
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
        };
    })

        //============================================


        //services.AddAuthentication(options =>
        //{
        //    options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
        //})
        //    .AddCookie(options =>
        //    {
        //        options.LoginPath = "/account/google-login";
        //    })


        .AddGoogle(options =>
                {
                    options.ClientId = "656369726729-i6fncd29kr7jfdsrjctt3bfud4m4ahc2.apps.googleusercontent.com";
                    options.ClientSecret = "w8zThmij2R4rJSH9Ih90RqYw";

                });

            services.AddDbContext<DakarisContext>
                (options => options.UseSqlServer(connection));

            services.AddMvc(options => options.EnableEndpointRouting = false).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            // Auto Mapper Configurations
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddScoped<INurseService, NurseService>();
            services.AddScoped<IPackageService, PackageService>();
            services.AddScoped<IHospitalService, HospitalService>();
            services.AddScoped<IAuthService, AuthService>();
            services.AddScoped<INotificationService, NotificationService>();

        }


        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                c.RoutePrefix = string.Empty;
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            //***
            app.UseAuthentication();
            app.UseAuthorization();
            //***
            app.UseHttpsRedirection();
            app.UseMvc();


        }
    }
}
