﻿using Dakaris.Model.Domain;
using Dakaris.Model.Dto.Nurse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Dakaris.Model.Dto.Complain;
using Dakaris.Model.Dto.Package;
using Dakaris.Model.Dto.Hospital;
using Dakaris.Model.Dto.ContactedNurse;
using Dakaris.Model.Dto.ViewedNurse;
using Dakaris.Model.Dto.Notification;

namespace Dakaris.Api.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            #region Nurse
            CreateMap<NurseDto, Nurse>()
                .ForMember(dest => dest.CurrentHospital, act => act.Ignore())
                .ForMember(dest => dest.Package, act => act.Ignore())
                .ForMember(dest => dest.ContacterList, act => act.Ignore())
                .ForMember(dest => dest.ContacteeList, act => act.Ignore())
                .ForMember(dest => dest.ViewerList, act => act.Ignore())
                .ForMember(dest => dest.VieweeList, act => act.Ignore());
            // .ForMember(dest => dest.NurseExpectingHospitalList, act => act.Ignore());
            
            CreateMap<NurseRegisterDto, Nurse>()
                .ForMember(dest => dest.CurrentHospital, act => act.Ignore())
                .ForMember(dest => dest.Package, act => act.Ignore())
                .ForMember(dest => dest.ContacterList, act => act.Ignore())
                .ForMember(dest => dest.ContacteeList, act => act.Ignore())
                .ForMember(dest => dest.ViewerList, act => act.Ignore())
                .ForMember(dest => dest.VieweeList, act => act.Ignore())
                .ForMember(dest => dest.NurseExpectingHospitalList, act => act.Ignore())                ;
            // .ForMember(dest => dest.NurseExpectingHospitalList, act => act.Ignore());



            CreateMap<Nurse, NurseDto>();


            #endregion

            #region Complain
            CreateMap<ComplainDto, Complain>();
            //.ForMember(dest => dest.Reporter, act => act.Ignore())
            //.ForMember(dest => dest.Reportee, act => act.Ignore())
            //.ForMember(dest => dest.CreatedAt, act => act.Ignore());

            CreateMap<Complain, ComplainDto>();
            #endregion

            #region Package
            CreateMap<PackageDto, Package>()
                .ForMember(dest => dest.Nurses, act => act.Ignore());

            CreateMap<Package, PackageDto>();
            #endregion

            #region Hospital
            CreateMap<HospitalDto, Hospital>()
                .ForMember(dest => dest.NurseExpectingHospitalList, act => act.Ignore())
                .ForMember(dest => dest.Nurses, act => act.Ignore());

            CreateMap<Hospital, HospitalDto>();
            #endregion

            #region NurseExpectingHospital
            CreateMap<NurseExpectingHospitalDto, NurseExpectingHospital>()
                .ForMember(dest => dest.Nurse, act => act.Ignore())
                .ForMember(dest => dest.Hospital, act => act.Ignore());

            CreateMap<NurseExpectingHospital, NurseExpectingHospitalDto>()
                .ForMember(dest => dest.HospitalName, act => act.MapFrom(src => src.Hospital.Name))
                .ForMember(dest => dest.District, act => act.MapFrom(src => src.Hospital.District));
            #endregion

            #region Admin
            CreateMap<AdminDto, Admin>();

            CreateMap<Admin, AdminDto>();
            #endregion

            #region ContactedNurse
            CreateMap<ContactedNurseDto, ContactedNurse>();

            CreateMap<ContactedNurse, ContactedNurseDto>();
            #endregion

            #region ViewedNurse
            CreateMap<ViewedNurseDto, ViewedNurse>();

            CreateMap<ViewedNurse, ViewedNurseDto>();
            #endregion

            #region Notification
            CreateMap<NotificationDto, Notification>()
                .ForMember(dest => dest.Nurse, act => act.Ignore());

            CreateMap<Notification, NotificationDto>();
            #endregion
        }
    }
}
