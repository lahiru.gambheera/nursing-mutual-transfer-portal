﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dakaris.Contracts.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Dakaris.Api.Controllers
{
    [EnableCors("MyPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationController : ControllerBase
    {
        private readonly INotificationService notificationService;

        public NotificationController(INotificationService notificationService)
        {
            this.notificationService = notificationService;
        }

        // GET: api/Notification
        //[HttpGet]
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        // GET: api/Notification/5
        [HttpGet("MyNotifications/{page}/{pageSize}")]
        public IActionResult Get(long page, long pageSize)
        {
            var id = getUserId();
            var notificationList = notificationService.GetMyNotificationList(id, page, pageSize);
            return Ok(notificationList);
        }

        // POST: api/Notification
       [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Notification/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(long id)
        {
            notificationService.DeleteNotification(id);
        }

        private long getUserId()
        {
            try
            {
                var userIdString = User.Claims.FirstOrDefault(c => c.Type == "UniqueId").Value;
                return Int64.Parse(userIdString);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}
