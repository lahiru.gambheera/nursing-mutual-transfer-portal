﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Dakaris.Contracts.Services;
using Dakaris.Model.Dto.User;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.Google;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Dakaris.Model.Dto.Nurse;
using Microsoft.Extensions.Configuration;

namespace Dakaris.Api.Controllers
{
    [EnableCors("MyPolicy")]
    [AllowAnonymous, Route("api/[controller]")]
    [ApiController]
    public class LoginController : Controller
    {
        private readonly IAuthService authService;
        private readonly INurseService nurseService;
        public IConfiguration configuration { get; }

        public LoginController(IAuthService authService, INurseService nurseService, IConfiguration configuration)
        {
            this.authService = authService;
            this.nurseService = nurseService;
            this.configuration = configuration;
        }

        [HttpPost("GoogleLogin")]
        public async Task<IActionResult> GoogleLogin([FromBody] GoogleUser googleUser)
        {
            var properties = new AuthenticationProperties { RedirectUri = Url.Action("GoogleResponse") };

            var isValidGoogleUser = await authService.VerifyGoogleTokenAsync(googleUser);

            if (!isValidGoogleUser) { return Unauthorized(); }

            var logedUser = await nurseService.isRegisteredAsync(googleUser);

            if (!logedUser.IsValidUser) { return Unauthorized(); }

            var token = GenerateJSONWebToken(googleUser, logedUser);

            var returningData = new LoginSuccessUser()
            {
                Token = token,
                IsRegistrationCompleted = logedUser.IsRegistered,
                UserId = logedUser.Id
            };

            return Ok(returningData);
        }

        [HttpPost("RefreshToken")]
        public async Task<IActionResult> RefreshToken([FromBody] RefreshToken refreshToken)
        {
            try
            {

            }
            catch (Exception ex)
            {

                throw;
            }
            return Ok();
        }

        private string GenerateJSONWebToken(GoogleUser googleUser, RegistrationInfoDto registeredUser)
        {
            try
            {
                var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:Key"]));
                var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

                var claims = new[] {
                        new Claim("UniqueId", registeredUser.Id.ToString()),
                        new Claim(JwtRegisteredClaimNames.Email, googleUser.Email),
                        new Claim("DateOfJoing", DateTime.UtcNow.ToString()),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                };

                var token = new JwtSecurityToken("DAKARIS",
                    "DAKARIS",
                    claims,
                    expires: DateTime.Now.AddMinutes(120),
                    signingCredentials: credentials);

                return new JwtSecurityTokenHandler().WriteToken(token);
            }
            catch (Exception ex)
            {

                throw;
            }

        }


        //[Route("google-response")]
        //public async Task<IActionResult> GoogleResponse()
        //{
        //    var result = await HttpContext.AuthenticateAsync(CookieAuthenticationDefaults.AuthenticationScheme);

        //    var claims = result.Principal.Identities.FirstOrDefault()
        //        .Claims.Select(claim => new
        //        {
        //            claim.Issuer,
        //            claim.OriginalIssuer,
        //            claim.Type,
        //            claim.Value
        //        });

        //    return Json(claims);
        //}

    }
}