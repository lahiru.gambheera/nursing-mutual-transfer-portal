﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dakaris.Contracts.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Dakaris.Api.Controllers
{
    [EnableCors("MyPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class HospitalController : ControllerBase
    {
        private readonly IHospitalService hospitalService;
        public HospitalController(IHospitalService hospitalService)
        {
            this.hospitalService = hospitalService;
        }

        // GET: api/Hospital
        [HttpGet]
        public IActionResult Get()
        {
            var result = hospitalService.GetAll();
            return Ok(result);
        }

        //// GET: api/Hospital/5
        //[HttpGet("{id}", Name = "Get")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST: api/Hospital
        //[HttpPost]
        //public void Post([FromBody] string value)
        //{
        //}

        //// PUT: api/Hospital/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
