﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dakaris.Contracts.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Dakaris.Api.Controllers
{
    [EnableCors("MyPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class PackageController : ControllerBase
    {
        private readonly IPackageService packageService;

        public PackageController(IPackageService packageService)
        {
            this.packageService = packageService;
        }

        [HttpGet("GetAllPackages")]
        public async Task<IActionResult> Get()
        {
            var result = await packageService.GetAllAsync();
            return Ok(result);
        }

        [HttpPost("{packageId}")]
        public async Task<IActionResult> Post(long packageId)
        {
            var result = await packageService.RequestForAPackageAsync(this.getUserId(), packageId);
            return Ok(result);
        }

        // POST: api/Package


        // PUT: api/Package/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        private long getUserId()
        {
            return 12;
        }
    }
}
