﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Dakaris.Contracts.Services;
using Dakaris.Model.Dto.Nurse;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Dakaris.Api.Controllers
{
    [EnableCors("MyPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    // [Authorize]
    public class NurseController : ControllerBase
    {
        private readonly INurseService nurseService;

        public NurseController(INurseService nurseService)
        {
            this.nurseService = nurseService;
        }

        //[HttpGet("Search/{fromHospital}/{toHospital}/{page}/{pageSize}")]
        //public async Task<IActionResult> Search(int fromHospital, int toHospital, int page, int pageSize)
        //{
        //    var result = await nurseService.SearchAsync(fromHospital, toHospital, page, pageSize);
        //    return Ok(result);
        //}

        [Authorize]
        [HttpGet("Search/{fromDistrict}/{toDistrict}/{page}/{pageSize}")]
        public async Task<IActionResult> Search(int fromDistrict, int toDistrict, int page, int pageSize)
        {
            getUserId();
            var result = await nurseService.SearchAsync(fromDistrict, toDistrict, page, pageSize);
            return Ok(result);
        }

        [HttpPost("PostNurse")]
        public async Task<IActionResult> Post([FromBody] NurseRegisterDto nurse)
        {
            var result = await nurseService.CreateAsync(nurse);
            return Ok(result);
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] NurseDto nurse)
        {
            var result = await nurseService.UpdateAsync(nurse);
            return Ok(result);
        }

        [HttpGet("{id}", Name = "Get")]
        public async Task<IActionResult> Get(string id)
        {
            var result = await nurseService.GetByIdAsync(id);
            return Ok(result);
        }

        [HttpGet("GetMyContactedNurseList")]
        public async Task<IActionResult> GetMyContactedNurseList()
        {
            var result = await nurseService.GetMyContactedNurseListAsync(getUserId());
            return Ok();
        }

        [HttpGet("GetMyData")]
        public async Task<IActionResult> GetMyData()
        {
            long userId = getUserId();
            var result = await nurseService.GetMyDataAsync(getUserId());
            return Ok(result);
        }

        [HttpGet("UnlockFullContact/{id}")]
        public async Task<IActionResult> UnlockFullContact(string id)
        {
            long userId = getUserId();
            var result = await nurseService.UnlockFullContact(userId, id);
            return Ok(result);
        }

        private long getUserId()
        {
            try
            {
                var userIdString = User.Claims.FirstOrDefault(c => c.Type == "UniqueId").Value;
                return Int64.Parse(userIdString);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}
