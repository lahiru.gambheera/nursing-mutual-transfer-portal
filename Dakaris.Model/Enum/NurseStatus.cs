﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dakaris.Model.Enum
{
    public enum NurseStatus
    {
        just_logged_in = 1,
        vacant = 2,
        finished = 3
    }
}
