﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dakaris.Model.Enum
{
    public enum NursePackageStatus
    {
        Pending = 1,
        Canceled = 2,
        Rejected = 3,
        Active = 4,
        CreditLimitExeeded = 5,
        Expired = 6,
    }
}
