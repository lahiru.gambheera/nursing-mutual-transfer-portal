﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Dakaris.Model.Enum
{
    public enum PackageTypes
    {
        [Display(Name ="Silver")]
        Silver = 1,
        [Display(Name = "Bronze")]
        Bronze = 2,
        [Display(Name = "Gold")]
        Gold = 3
    }
}
