﻿
using System;
using System.Collections.Generic;

public class CustomerOrder
{
    public string OrderNo { get; set; }
    public string CustomerCode { get; set; }
    public string CustomerName { get; set; }
    public string Status { get; set; }
    public List<PackageDetail> PackageDetails { get; set; } = new List<PackageDetail>();
    public string Warehouse { get; set; }
    public string Reference { get; set; }
    public DateTime? OrderDate { get; set; }
    public DateTime? DateWanted { get; set; }
    public string JobReference { get; set; }
    public string CustomerReference { get; set; }
    public string CarrierCode { get; set; }
    public string CarrierName { get; set; }
    public string TrackingNumber { get; set; }
    public Receiver Receiver { get; set; }
    public string Instructions { get; set; }
    public string Notes { get; set; }
    public List<CustomerOrderLineResponse> CustomerOrderLines { get; set; } = new List<CustomerOrderLineResponse>();
    public decimal SubTotal { get; set; }
    public decimal ExtraCharges { get; set; }
    public decimal Tax { get; set; }
    public decimal Total { get; set; }
}

public class Receiver
{
    public string Code { get; set; }
    public string Name { get; set; }
    public string Address { get; set; }
    public string Suburb { get; set; }
    public string StateCode { get; set; }
    public string PostCode { get; set; }
    public string CountryCode { get; set; }
    public string Telephone { get; set; }
    public string Email { get; set; }

}

public class CustomerOrderResponse
{
    public Result Result { get; set; }

    public List<CustomerOrder> CustomerOrders { get; set; } = new List<CustomerOrder>();
}

public class Result
{
    public int ErrorCode { get; set; }
    public string AdditionalInfo { get; set; }
}

public class CustomerOrderLineResponse
{
    public long LineNumber { get; set; }
    public string ProductCode { get; set; }

    public string ProductDescription { get; set; }
    public string UOM { get; set; }
    public decimal Length { get; set; }
    public decimal Width { get; set; }
    public decimal Height { get; set; }
    public decimal Weight { get; set; }
    public decimal QtyOrdered { get; set; }
    public decimal QtyShipped { get; set; }
    public List<BatchLine> BatchLines { get; set; } = new List<BatchLine>();
}

public class BatchLine
{
    public string SerialBatch { get; set; }
    public DateTime UseByDate { get; set; }
}

public class PackageDetail
{
    public decimal Quantity { get; set; }
    public string Package { get; set; }
    public decimal Length { get; set; }
    public decimal Width { get; set; }
    public decimal Height { get; set; }
    public decimal Weight { get; set; }

}