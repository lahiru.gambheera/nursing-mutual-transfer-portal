﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dakaris.Model.Domain
{
    public class Notification
    {
        public long Id { get; set; }
        public long NurseId { get; set; }
        public Nurse Nurse { get; set; }
        public DateTime Time { get; set; }
        public string Message { get; set; }
        public string HiddenMessage { get; set; }
        public bool IsRead { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActionButtonAvailable { get; set; }
        public string ActionButtonText { get; set; }
        public string ActionButtonLink { get; set; }
    }
}
