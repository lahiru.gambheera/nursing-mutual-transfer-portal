﻿using Dakaris.Model.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dakaris.Model.Domain
{
    public class PackageRequest
    {
        public long Id { get; set; }
        public long NurseId { get; set; }
        public virtual Nurse Nurse { get; set; }

        public long PackageId { get; set; }
        public virtual Package Package { get; set; }

        public NursePackageStatus Status { get; set; }
        public DateTime RequestedAt { get; set; }
    }
}
