﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dakaris.Model.Domain
{
    public class NurseExpectingHospital
    {
        public long Id { get; set; }
        
        public long NurseId { get; set; }
        public virtual Nurse Nurse { get; set; }

        public long hospitalId { get; set; }
        public virtual Hospital Hospital { get; set; }
    }
}
