﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dakaris.Model.Domain
{
    public class Complain
    {
        public long Id { get; set; }
        public long ReporterId { get; set; }
        public Nurse Reporter { get; set; }
        public long ReporteeId { get; set; }
        public Nurse Reportee { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;
    }
}
