﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dakaris.Model.Domain
{
    public class ContactedNurse
    {
        public long Id { get; set; }

        public long ContacterId { get; set; }
        public virtual Nurse Contacter { get; set; }

        public long ContacteeId { get; set; }
        public virtual Nurse Contactee { get; set; }

        public DateTime CreatedAt => DateTime.UtcNow;
    }
}
