﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dakaris.Model.Domain
{
    public class Admin
    {
        public long Id { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
    }
}
