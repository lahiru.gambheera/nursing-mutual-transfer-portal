﻿using Dakaris.Model.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dakaris.Model.Domain
{
    public class Hospital
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public District District { get; set; }
        public bool IsActive { get; set; }

        public virtual List<Nurse> Nurses { get; set; }
        public virtual List<NurseExpectingHospital> NurseExpectingHospitalList { get; set; }
    }
}
