﻿using Dakaris.Model.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dakaris.Model.Domain
{
    public class Nurse
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Nic { get; set; }
        public string MobileNumber { get; set; }
        public string ExtraMobileNo { get; set; }
        public string Email { get; set; }
        public string GoogleId { get; set; }
        public NurseStatus Status { get; set; }

        public string MyPromoCode { get; set; }
        public string RegisteredPromoCode { get; set; }
        public bool HasPaidForPromo { get; set; }

        public long? CurrentHospitalId { get; set; }
        public virtual Hospital? CurrentHospital { get; set; }

        public long PackageId { get; set; }
        public virtual Package Package { get; set; }

        public bool IsActive { get; set; }
        public bool IsSuspended { get; set; }

        public int AvailableViewCredits { get; set; }
        public long ViewCount { get; set; }

        public DateTime CreatedAt => DateTime.UtcNow;

        public virtual List<ContactedNurse> ContacterList { get; set; }
        public virtual List<ContactedNurse> ContacteeList { get; set; }

        public virtual List<ViewedNurse> ViewerList { get; set; }
        public virtual List<ViewedNurse> VieweeList { get; set; }

        public virtual List<Complain> ReporterList { get; set; }
        public virtual List<Complain> ReporteeList { get; set; }

        public virtual List<NurseExpectingHospital> NurseExpectingHospitalList { get; set; }

        public virtual List<PackageRequest> PackageRequestList { get; set; }
        public virtual List<Notification> NotificationList { get; set; }

    }
}
