﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dakaris.Model.Domain
{
    public class Package
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public int MaxViewCount { get; set; }
        public bool IsActive { get; set; }

        public virtual List<Nurse> Nurses { get; set; }
        public virtual List<PackageRequest> PackageRequestList { get; set; }
    }
}
