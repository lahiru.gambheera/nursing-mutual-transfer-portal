﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dakaris.Model.Domain
{
    public class GirlyName
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string AvatarUrl { get; set; }
    }
}
