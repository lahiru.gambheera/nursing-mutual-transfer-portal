﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dakaris.Model.Domain
{
    public class ViewedNurse
    {
        public long Id { get; set; }

        public long ViewerId { get; set; }
        public virtual Nurse Viewer { get; set; }

        public long VieweeId { get; set; }
        public virtual Nurse Viewee { get; set; }

        public DateTime CreatedAt => DateTime.UtcNow;
    }
}
