﻿using Dakaris.Model.Dto.Nurse;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Dakaris.Model.Dto.Complain
{
    public class ComplainDto
    {
        public long Id { get; set; }
        public long ReporterId { get; set; }
        public NurseDto Reporter { get; set; }
        [Required]
        public long ReporteeId { get; set; }
        public NurseDto Reportee { get; set; }
        [Required]
        public string Description { get; set; }
        public bool IsActive { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}")]
        public DateTime CreatedAt { get; set; }
    }
}
