﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dakaris.Model.Dto.User
{
    public class RefreshToken
    {
        public string MyRefreshToken { get; set; }
    }
}
