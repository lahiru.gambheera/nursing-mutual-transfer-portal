﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dakaris.Model.Dto.User
{
    public class LoginSuccessUser
    {
        public string Token { get; set; }
        public bool IsRegistrationCompleted { get; set; }
        public long UserId { get; set; }
    }
}
