﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dakaris.Model.Dto.User
{
    public class GoogleUser
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string UserId { get; set; }
        public string Token { get; set; }
    }
}
