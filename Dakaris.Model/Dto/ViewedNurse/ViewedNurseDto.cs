﻿using Dakaris.Model.Dto.Nurse;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dakaris.Model.Dto.ViewedNurse
{
    public class ViewedNurseDto
    {
        public long Id { get; set; }

        public long ViewerId { get; set; }
        public NurseDto Viewer { get; set; }

        public long VieweeId { get; set; }
        public NurseDto Viewee { get; set; }
    }
}
