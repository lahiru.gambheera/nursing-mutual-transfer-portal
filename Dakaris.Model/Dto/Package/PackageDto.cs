﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Dakaris.Model.Enum;

namespace Dakaris.Model.Dto.Package
{
    public class PackageDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public int MaxViewCount { get; set; }
        public bool IsActive { get; set; }
    }
}
