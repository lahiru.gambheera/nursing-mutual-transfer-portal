﻿using Dakaris.Model.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dakaris.Model.Dto.Hospital
{
    public class HospitalDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public District District { get; set; }
        public bool IsActive { get; set; }
    }
}
