﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dakaris.Model.Dto.Nurse
{
    public class NurseRegisterDto
    {
        public long Id { get; set; }
        public string EncriptedId { get; set; }
        public string Name { get; set; }
        // [RegularExpression("^([0-9]{9}[x|X|v|V]|[0-9]{12})$", ErrorMessage = "Not valid format (9 digit or 12 digit)")]
        public string Nic { get; set; }
        // [RegularExpression(@"^7|0|(?:\+94)[0-9]{9,10}$", ErrorMessage = "Not valid format eg:+947xxxxxxxx")]
        public string MobileNumber { get; set; }
        // [DataType(DataType.PhoneNumber)]
        public string ExtraMobileNo { get; set; }
        public string Email { get; set; }
        public string RegisteredPromoCode { get; set; }
        public long CurrentHospitalId { get; set; }
        public List<long> NurseExpectingHospitalList { get; set; }
    }
}
