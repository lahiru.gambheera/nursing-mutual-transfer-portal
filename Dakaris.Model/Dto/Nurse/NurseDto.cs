﻿using Dakaris.Model.Dto.Hospital;
using Dakaris.Model.Dto.Package;
using Dakaris.Model.StaticData;
using Dakaris.Utility.Encriptors;
using Dakaris.Utility.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Dakaris.Model.Dto.Nurse
{
    public class NurseDto
    {
        public long Id { get; set; }
        public string EncriptedId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        // [RegularExpression("^([0-9]{9}[x|X|v|V]|[0-9]{12})$", ErrorMessage = "Not valid format (9 digit or 12 digit)")]
        public string Nic { get; set; }
        [Required]
        // [RegularExpression(@"^7|0|(?:\+94)[0-9]{9,10}$", ErrorMessage = "Not valid format eg:+947xxxxxxxx")]
        public string MobileNumber { get; set; }
        // [DataType(DataType.PhoneNumber)]
        public string ExtraMobileNo { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public string MyPromoCode { get; set; }
        public string RegisteredPromoCode { get; set; }
        public bool HasPaidForPromo { get; set; }
        [Required]
        public long CurrentHospitalId { get; set; }
        [Required]
        public long PackageId { get; set; }

        public int AvailableViewCredits { get; set; }

        public bool IsActive { get; set; }
        public bool IsSuspended { get; set; }
        public string AvatarUrl { get; set; }

        public PackageDto Package { get; set; }
        public HospitalDto CurrentHospital { get; set; }
        public List<NurseExpectingHospitalDto> NurseExpectingHospitalList { get; set; }

        public void EncriptAllSensitiveData()
        {
            Name = GirlyName.GetGirlyName(Id);
            EncriptedId = Id > 0 ? Encriptor.EncryptFromLong(Id, EncriptObjectType.Nurse) : null;
            Id = 0;
            Email = "xxxx@xxx.com";
            MobileNumber = "07x x xxx xxx";
            ExtraMobileNo = "07x x xxx xxx";
        }

        public void EncryptId()
        {
            EncriptedId = Id > 0 ? Encriptor.EncryptFromLong(Id, EncriptObjectType.Nurse) : null;
            Id = 0;
        }

        public void DecryptId()
        {
            Id = EncriptedId != null ? Encriptor.DecryptToLong(EncriptedId, EncriptObjectType.Nurse) : 0;
            EncriptedId = null;
        }
    }
}
