﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dakaris.Model.Dto.Nurse
{
    public class PreviouslyContactedNurse
    {
        public string EncriptedId { get; set; }
        public string Name { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string CurrentHospital { get; set; }
        public List<string> ExpectedHospitalList { get; set; }
    }
}
