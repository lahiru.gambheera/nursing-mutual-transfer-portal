﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dakaris.Model.Dto.Nurse
{
    public class AdminDto
    {
        public long Id { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
    }
}
