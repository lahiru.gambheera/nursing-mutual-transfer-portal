﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dakaris.Model.Dto.Nurse
{
    public class RegistrationInfoDto
    {
        public long Id { get; set; }
        public bool IsRegistered { get; set; }
        public bool IsValidUser { get; set; }
    }
}
