﻿using Dakaris.Model.Dto.Hospital;
using Dakaris.Model.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dakaris.Model.Dto.Nurse
{
    public class NurseExpectingHospitalDto
    {
        public long Id { get; set; }

        public long NurseId { get; set; }

        public long HospitalId { get; set; }

        public string HospitalName { get; set; }
        public District District { get; set; }
    }
}
