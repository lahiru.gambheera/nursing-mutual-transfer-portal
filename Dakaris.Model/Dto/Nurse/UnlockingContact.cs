﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dakaris.Model.Dto.Nurse
{
    public class UnlockingContact
    {
        public bool IsUnlocked { get; set; }
        public string Message { get; set; }
        public PreviouslyContactedNurse Contactee { get; set; }
    }
}
