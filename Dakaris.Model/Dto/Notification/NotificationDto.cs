﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dakaris.Model.Dto.Notification
{
    public class NotificationDto
    {
        public long Id { get; set; }
        public long NurseId { get; set; }
        public DateTime Time { get; set; }
        public string Message { get; set; }
        public bool IsRead { get; set; }
        public bool IsActionButtonAvailable { get; set; }
        public string ActionButtonText { get; set; }
        public string ActionButtonLink { get; set; }
    }
}
