﻿using Dakaris.Model.Dto.Nurse;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dakaris.Model.Dto.ContactedNurse
{
    public class ContactedNurseDto
    {
        public long Id { get; set; }

        public long ContacterId { get; set; }
        public NurseDto Contacter { get; set; }

        public long ContacteeId { get; set; }
        public NurseDto Contactee { get; set; }
    }
}
