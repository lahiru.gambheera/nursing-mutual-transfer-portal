﻿using Dakaris.Client.Helper.CustomComponent;
using Dakaris.Contracts.Services;
using Dakaris.Model.Dto.Complain;
using Dakaris.Model.Dto.Nurse;
using Dakaris.Model.Enum;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace Dakaris.Client.Pages.Admin
{
    public partial class Dashboard
    {
        [Inject]
        public INurseService _nurseService { get; set; }
        [Inject]
        public IComplainService _complainService { get; set; }

        PieChart pieChartRegisterNurses;
        PieChart pieChartPackagesofNurses;
        PieChart pieChartMutualTransfers;
        List<ChartDataItem> RegisterNursesList = new List<ChartDataItem>();
        List<ChartDataItem> PackagesofNursesList = new List<ChartDataItem>();
        List<ChartDataItem> MutualTransfersList = new List<ChartDataItem>();

        List<NurseDto> nurseDtosList = new List<NurseDto>();
        List<ComplainDto> complainDtosList = new List<ComplainDto>();

        protected override async Task OnInitializedAsync()
        {
            ChartDataItem chartDataItem;
            nurseDtosList = await _nurseService.GetAllAsync();
            complainDtosList = await _complainService.GetAllAsync();

            /////////////////////////////////////////////////////////////////
            #region Register Nurses
            chartDataItem = new ChartDataItem();
            chartDataItem.LabelName = "Active Nurses Count";
            chartDataItem.Value = nurseDtosList.Count(a=>a.IsActive == true && a.IsSuspended == false);
            chartDataItem.Color = Color.LimeGreen;
            RegisterNursesList.Add(chartDataItem);

            chartDataItem = new ChartDataItem();
            chartDataItem.LabelName = "DeActive Nurses Count";
            chartDataItem.Value = nurseDtosList.Count(a => a.IsActive == false && a.IsSuspended == false);
            chartDataItem.Color = Color.DarkOrange;
            RegisterNursesList.Add(chartDataItem);

            chartDataItem = new ChartDataItem();
            chartDataItem.LabelName = "Suspend Nurses Count";
            chartDataItem.Value = nurseDtosList.Count(a => a.IsSuspended == true);
            chartDataItem.Color = Color.Crimson;
            RegisterNursesList.Add(chartDataItem);
            pieChartRegisterNurses = new PieChart();
            #endregion

            /////////////////////////////////////////////////////////////////////
            #region Packages of Nurses
            chartDataItem = new ChartDataItem();
            chartDataItem.LabelName = "Silver Package Users";
            chartDataItem.Value = nurseDtosList.Count(a => a.PackageId == 1);
            chartDataItem.Color = Color.SlateGray;
            PackagesofNursesList.Add(chartDataItem);

            chartDataItem = new ChartDataItem();
            chartDataItem.LabelName = "Bronze Package Users";
            chartDataItem.Value = nurseDtosList.Count(a => a.PackageId == 2);
            chartDataItem.Color = Color.Coral;
            PackagesofNursesList.Add(chartDataItem);

            chartDataItem = new ChartDataItem();
            chartDataItem.LabelName = "Gold Package Users";
            chartDataItem.Value = nurseDtosList.Count(a => a.PackageId == 3);
            chartDataItem.Color = Color.Gold;
            PackagesofNursesList.Add(chartDataItem);
            pieChartPackagesofNurses = new PieChart();
            #endregion

            ///////////////////////////////////////////////////////////////////////
            #region Mutual Transfers
            chartDataItem = new ChartDataItem();
            chartDataItem.LabelName = "Less than 3 Complains";
            chartDataItem.Value = complainDtosList.GroupBy(a => a.ReporteeId).Select(grp => grp.ToList()).Count(a => a.Count < 4);
            chartDataItem.Color = Color.DeepSkyBlue;
            MutualTransfersList.Add(chartDataItem);

            chartDataItem = new ChartDataItem();
            chartDataItem.LabelName = "Between 4 and 10 Complains";
            chartDataItem.Value = complainDtosList.GroupBy(a => a.ReporteeId).Select(grp => grp.ToList()).Count(a => a.Count >3 && a.Count < 11);
            chartDataItem.Color = Color.SpringGreen;
            MutualTransfersList.Add(chartDataItem);

            chartDataItem = new ChartDataItem();
            chartDataItem.LabelName = "More than 10 Complains";
            chartDataItem.Value = complainDtosList.GroupBy(a => a.ReporteeId).Select(grp => grp.ToList()).Count(a => a.Count > 10);
            chartDataItem.Color = Color.DeepPink;
            MutualTransfersList.Add(chartDataItem);
            pieChartMutualTransfers = new PieChart();
            #endregion



            //StateHasChanged();
        }
    }
}
