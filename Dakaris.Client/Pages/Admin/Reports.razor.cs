﻿using Dakaris.Contracts.Services;
using Dakaris.Model.Dto.Hospital;
using Dakaris.Model.Dto.Nurse;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using Microsoft.JSInterop;
using IronPdf;
using System.Text;
using System.Web;
using System.Net;

namespace Dakaris.Client.Pages.Admin
{
    public partial class Reports
    {
        //private readonly IJSRuntime _jsRuntime;

        [Inject]
        public INurseService _nurseService { get; set; }
        [Inject]
        public IHospitalService _hospitalService { get; set; }
        [Inject]
        public INurseExpectingHospitalService _nurseExpectingHospitalService { get; set; }
        [Inject]
        IJSRuntime _jsRuntime { get; set; }
        private EditContext EditContext { get; set; }
        public List<NurseDto> nurseList = new List<NurseDto>();
        public List<HospitalDto> hospitalList = new List<HospitalDto>();
        public List<NurseExpectingHospitalDto> nurseExpectingHospitalList = new List<NurseExpectingHospitalDto>();
        protected List<HospitalDto> nurseExpectList;

        string RegDivShow = "none";
        string UnRegDivShow = "none";

        //public Reports(IJSRuntime jsRuntime)
        //{
        //    _jsRuntime = jsRuntime;
        //}

        protected override async Task OnInitializedAsync()
        {
            await ListLoad();
        }

        private async Task ListLoad()
        {
            nurseList = await _nurseService.GetAllAsync();
            nurseExpectingHospitalList = await _nurseExpectingHospitalService.GetAllAsync();
            hospitalList = await _hospitalService.GetAllAsync();
        }

        public List<HospitalDto> GetExpectHospitalByNurseIdAsync(long nurseId)
        {
            List<HospitalDto> hospitalList = new List<HospitalDto>();
            hospitalList = (from hospital in this.hospitalList
                            join joinHospital in this.nurseExpectingHospitalList
                            on hospital.Id equals joinHospital.HospitalId
                            where joinHospital.NurseId == nurseId
                            select hospital).ToList();

            return hospitalList;
        }

        public void ChangeNurseActiveStatus(long nurseId, string status)
        {
            if (status == "Active")
            {
                _nurseService.ActivateAcountAsync(nurseId);
            }
            else
            {
                _nurseService.SuspendAcountAsync(nurseId);
            }
        }

        public void ExportFile(char type)
        {
            string html = "";
            if (type.Equals('A'))
            {
                html = pdfHtmlGenerator(nurseList.Where(a => a.IsActive == true).Take(30).OrderByDescending(a => a.Id).ToList(), type);
            }
            if (type.Equals('B'))
            {
                html = pdfHtmlGenerator(nurseList.Where(a => a.IsActive == false).Take(30).OrderByDescending(a => a.Id).ToList(), type);
            }

            // Render any HTML fragment or document to HTML
            var Renderer = new IronPdf.HtmlToPdf();
            Renderer.PrintOptions.DPI = 300;
            Renderer.PrintOptions.EnableJavaScript = true;
            Renderer.PrintOptions.CustomCssUrl = @"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css";
            Renderer.PrintOptions.GrayScale = true;

            var PDF = Renderer.RenderHtmlAsPdf(html);
            string dd = DateTime.Now.ToString("dd-MM-yyyy_HH-mm-ss");
            var OutputPath = "Report" + dd + ".pdf";
            GetPdf(OutputPath, PDF, _jsRuntime);
        }

        public void GetPdf(string fileName, PdfDocument pdf, IJSRuntime js)
        {
            js.InvokeAsync<object>(
                           "saveAsPdfFile",
                           fileName,
                           Convert.ToBase64String(pdf.Stream.ToArray()));
        }

        private string pdfHtmlGenerator(List<NurseDto> nurseList, char type)
        {
            var outHtml = new StringBuilder();

            if (type.Equals('A'))
            {
                outHtml.Append(@"
                        <html>
                            <head>
                            </head>
                            <body>
                                <div class='header'><h4 class='text-danger'>Recently Activated Users Records (Top 30)</h4></div>");
            }
            if (type.Equals('B'))
            {
                outHtml.Append(@"
                        <html>
                            <head>
                            </head>
                            <body>
                                <div class='header'><h4 class='text-danger'>Recently InActivated Users Records (Top 30)</h4></div>");
            }
            outHtml.Append(@"
                                <table class='table'>
                                    <thead>
                                        <tr>
                                            <th> Nurse Name </th>
                                            <th> Mobile No </th>
                                            <th> NIC </th>
                                            <th> Email </th>
                                        </tr>
                                    </thead>
                                    <tbody> ");
            foreach (NurseDto nurse in nurseList)
            {
                outHtml.AppendFormat(@"<tr>
                                            <td>{0}</td>
                                            <td>{1}</td>
                                            <td>{2}</td>
                                            <td>{3}</td>
                                          </tr>", nurse.Name, nurse.MobileNumber, nurse.Nic, nurse.Email);

            }
            outHtml.Append(@"
                                </table>
                            </body>
                        </html>");

            return outHtml.ToString();
        }
        public void ShowHide(char type)
        {
            if (type.Equals('A'))
            {
                RegDivShow = "block";
                UnRegDivShow = "none";
            }
            if (type.Equals('B'))
            {
                UnRegDivShow = "block";
                RegDivShow = "none";
            }
        }
    }
}
