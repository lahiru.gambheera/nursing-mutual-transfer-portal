﻿using Dakaris.Contracts.Services;
using Dakaris.Client.Helper.CustomComponent;
using Dakaris.Model.Dto.Complain;
using Dakaris.Model.Dto.Nurse;
using Dakaris.Model.Enum;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using ChartJs.Blazor.ChartJS.BarChart;

namespace Dakaris.Client.Pages.Admin
{
    public partial class Charts
    {
        [Inject]
        public INurseService _nurseService { get; set; }

        BarChart packageUsage;
        List<ChartDataItem> PackagesofNursesList = new List<ChartDataItem>();

        List<NurseDto> nurseDtosList = new List<NurseDto>();
        private ChartDataItem chartDataItem;

        protected override async Task OnInitializedAsync()
        {
            nurseDtosList = await _nurseService.GetAllAsync();
            //nurseDtosList.Where(a=>a.is)
            #region Packages of Nurses
            chartDataItem = new ChartDataItem();
            chartDataItem.LabelName = "Silver Package Users";
            chartDataItem.Value = nurseDtosList.Count(a => a.PackageId == 1);
            chartDataItem.Color = Color.SlateGray;
            PackagesofNursesList.Add(chartDataItem);

            chartDataItem = new ChartDataItem();
            chartDataItem.LabelName = "Bronze Package Users";
            chartDataItem.Value = nurseDtosList.Count(a => a.PackageId == 2);
            chartDataItem.Color = Color.Coral;
            PackagesofNursesList.Add(chartDataItem);

            chartDataItem = new ChartDataItem();
            chartDataItem.LabelName = "Gold Package Users";
            chartDataItem.Value = nurseDtosList.Count(a => a.PackageId == 3);
            chartDataItem.Color = Color.Gold;
            PackagesofNursesList.Add(chartDataItem);
            packageUsage = new BarChart();
            #endregion
        }
    }
}
