﻿using Dakaris.Contracts.Services;
using Dakaris.Model.Dto.Hospital;
using Dakaris.Model.Dto.Nurse;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dakaris.Client.Pages.Admin
{
    public partial class ViewNurse
    {
        [Inject]
        public INurseService _nurseService { get; set; }
        [Inject]
        public IHospitalService _hospitalService { get; set; }
        [Inject]
        public INurseExpectingHospitalService _nurseExpectingHospitalService { get; set; }
        private EditContext EditContext { get; set; }
        public List<NurseDto> nurseList = new List<NurseDto>();
        public List<HospitalDto> hospitalList = new List<HospitalDto>();
        public List<NurseExpectingHospitalDto> nurseExpectingHospitalList = new List<NurseExpectingHospitalDto>();
        protected List<HospitalDto> nurseExpectList;


        protected override async Task OnInitializedAsync()
        {
            await ListLoad();
        }

        private async Task ListLoad()
        {
            nurseList = await _nurseService.GetAllAsync();
            nurseExpectingHospitalList = await _nurseExpectingHospitalService.GetAllAsync();
            hospitalList = await _hospitalService.GetAllAsync();
        }

        public List<HospitalDto> GetExpectHospitalByNurseIdAsync(long nurseId)
        {
            List<HospitalDto> hospitalList = new List<HospitalDto>();
            hospitalList = (from hospital in this.hospitalList
                            join joinHospital in this.nurseExpectingHospitalList
                            on hospital.Id equals joinHospital.HospitalId
                            where joinHospital.NurseId == nurseId
                            select hospital).ToList();

            return hospitalList;
        }

        public void ChangeNurseActiveStatus(long nurseId, string status)
        {
            if(status == "Active")
            {
                _nurseService.ActivateAcountAsync(nurseId);
            }
            else
            {
                _nurseService.SuspendAcountAsync(nurseId);
            }
        }
    }
}
