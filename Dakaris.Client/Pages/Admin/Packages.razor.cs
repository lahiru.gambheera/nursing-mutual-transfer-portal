﻿using Dakaris.Contracts.Services;
using Dakaris.Model.Dto.Package;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dakaris.Client.Pages.Admin
{
    public partial class Packages
    {
        [Inject]
        public IPackageService _packageService { get; set; }
        //public bool ShowList { get; set; } = true;
        //private string FromType { get; set; } = "Update";
        protected string OkayDisabled { get; set; } = "disabled";
        protected string FieldDisabled { get; set; } = "disabled";
        //private string delMsgShow { get; set; }
        //private long delMsgId { get; set; }
        private EditContext EditContext { get; set; }
        public List<PackageDto> packageList = new List<PackageDto>();
        public PackageDto packageDto = new PackageDto();

        public Packages()
        {
            //ShowList = true;
        }

        //protected override async Task OnInitializedAsync()
        //{
        //    //await ListLoad();
        //}

        //public async Task GetDetailViewAsync(long Id, string formType)
        //{
        //    if (Id != 0)
        //    {
        //        packageDto = await _packageService.GetByIdAsync(Id);
        //    }
        //    else
        //    {
        //        packageDto = new PackageDto();
        //    }

        //    FromType = formType;
        //    ShowList = false;
        //}

        protected async Task SubmitAsync(PackageDto packageDto)
        {
            if(packageDto != null)
            {
                if(packageDto.Id == 0)
                {
                    await _packageService.CreateAsync(packageDto);
                }
                else
                {
                    await _packageService.UpdateAsync(packageDto);
                }

                FormReset();
                //await ListLoad();
            }
        }
        
        //protected async Task DeleteRecord(long Id)
        //{
        //    if (Id != 0)
        //    {
        //        packageDto = await _packageService.GetByIdAsync(Id);
        //        await _packageService.DeleteAsync(packageDto);
        //        await ListLoad();
        //    }
        //}

        public void FormReset()
        {
            packageDto = new PackageDto();
            OkayDisabled = "disabled";
            FieldDisabled = "disabled";
        }

        //public void FormCancel()
        //{
        //    _ = ListLoad();
        //}

        protected override bool ShouldRender()
        {
            var renderUI = true;

            return renderUI;
        }

        protected override void OnAfterRender(bool firstRender)
        {
            base.OnAfterRender(firstRender);
            EditContext = new EditContext(packageDto);
            SetOkDisabledStatus();
        }

        private void EditContext_OnFieldChanged(object sender, FieldChangedEventArgs e)
        {
            SetOkDisabledStatus();
        }

        private void SetOkDisabledStatus()
        {
            if (EditContext.Validate())
            {
                OkayDisabled = null;
            }
            else
            {
                OkayDisabled = "disabled";
            }
        }

        //public void clickDel(long id)
        //{
        //    delMsgId = id;
        //    delMsgShow = "block";
        //}

        //private async Task ListLoad()
        //{
        //    packageList = await _packageService.GetAllAsync();
        //    ShowList = true;
        //    delMsgId = 0;
        //    delMsgShow = "none";
        //}

        public async Task packageSelectionChange(ChangeEventArgs e)
        {
            if (e.Value.ToString() != "default")
            {
                packageDto = await _packageService.GetByNameAsync(e.Value.ToString());
                if(packageDto == null)
                {
                    packageDto = new PackageDto();
                    packageDto.Name = e.Value.ToString();
                    packageDto.Id = 0;
                }
                FieldDisabled = null;
            }
            else
            {
                FormReset();
            }
        }
    }
}
