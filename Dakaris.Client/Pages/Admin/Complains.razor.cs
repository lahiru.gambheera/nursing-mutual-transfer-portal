﻿using Dakaris.Contracts.Services;
using Dakaris.Model.Dto.Complain;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dakaris.Client.Pages.Admin
{
    public partial class Complains
    {
        [Inject]
        public IComplainService _complainService { get; set; }
        private EditContext EditContext { get; set; }
        public List<ComplainDto> complainList = new List<ComplainDto>();

        protected override async Task OnInitializedAsync()
        {
            await ListLoad();
        }

        private async Task ListLoad()
        {
            complainList = await _complainService.GetAllAsync();
            complainList = complainList.Where(a => a.IsActive == true).ToList();
            StateHasChanged();
        }

        public async Task ChangeComplainActiveStatusAsync(long nurseId)
        {
            await _complainService.DeactivateComplainAsync(nurseId);
            await ListLoad();
        }
    }
}
