﻿using Dakaris.Contracts.Services;
using Dakaris.Model.Dto.Hospital;
using Dakaris.Model.Dto.Nurse;
using Dakaris.Model.Dto.ViewedNurse;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Dakaris.Client.Pages
{
    public partial class Profile_Nurse
    {
        [Inject]
        public INurseService _nurseService { get; set; }
        [Inject]
        public IHospitalService _hospitalService { get; set; }
        [Inject]
        public INurseExpectingHospitalService _nurseExpectingHospitalService { get; set; }
        [Inject]
        public IViewedNurseService _viewedNurseService { get; set; }
        [CascadingParameter]
        private Task<AuthenticationState> authenticationStateTask { get; set; }
        private EditContext EditContext { get; set; }
        NurseDto loggedNurseDto = new NurseDto();
        List<HospitalDto> hospitalList = new List<HospitalDto>();
        List<NurseDto> nurseSearchlList = new List<NurseDto>();
        List<NurseExpectingHospitalDto> nurseExpectingHospitalList = new List<NurseExpectingHospitalDto>();
        List<ViewedNurseDto> viewedNurseList = new List<ViewedNurseDto>();
        protected string OkayDisabled { get; set; } = "disabled";
        int availableViewCount = 0;

        protected override async Task OnInitializedAsync()
        {
            
            var user = (await authenticationStateTask).User;
            if (user.Identity.IsAuthenticated)
            {
                loggedNurseDto = await _nurseService.GetByEmailAsync(user.FindFirstValue(ClaimTypes.Email).ToLower());
            }
            else
            {
                loggedNurseDto = new NurseDto();
            }
            await ListLoad();
            EditContext = new EditContext(loggedNurseDto);
        }

        private async Task ListLoad()
        {
            nurseExpectingHospitalList = await _nurseExpectingHospitalService.GetAllAsync();
            hospitalList = await _hospitalService.GetAllAsync();
            viewedNurseList = await _viewedNurseService.GetByViewerIdAsync(loggedNurseDto.Id);
            availableViewCount = loggedNurseDto.Package.MaxViewCount - viewedNurseList.Count;
        }

        public List<HospitalDto> GetExpectHospitalByNurseIdAsync(long nurseId)
        {
            List<HospitalDto> hospitalList = new List<HospitalDto>();
            hospitalList = (from hospital in this.hospitalList
                            join joinHospital in this.nurseExpectingHospitalList
                            on hospital.Id equals joinHospital.HospitalId
                            where joinHospital.NurseId == nurseId
                            select hospital).ToList();

            return hospitalList;
        }

        protected async Task SubmitAsync(NurseDto nurseDto)
        {
            if (nurseDto != null)
            {
                if (nurseDto.Id > 0)
                {
                    await _nurseService.UpdateAsync(nurseDto);
                }

            }
        }

        protected override bool ShouldRender()
        {
            var renderUI = true;

            return renderUI;
        }

        protected override void OnAfterRender(bool firstRender)
        {
            base.OnAfterRender(firstRender);
            if(EditContext != null)
            {
                SetOkDisabledStatus();
            }
        }

        private void EditContext_OnFieldChanged(object sender, FieldChangedEventArgs e)
        {
            SetOkDisabledStatus();
        }

        private void SetOkDisabledStatus()
        {
            if (EditContext.Validate())
            {
                OkayDisabled = null;
            }
            else
            {
                OkayDisabled = "disabled";
            }
            StateHasChanged();
        }


    }
}
