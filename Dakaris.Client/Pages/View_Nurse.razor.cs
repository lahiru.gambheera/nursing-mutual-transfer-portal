﻿using Dakaris.Contracts.Services;
using Dakaris.Model.Dto.Hospital;
using Dakaris.Model.Dto.Nurse;
using Dakaris.Model.Dto.ViewedNurse;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Dakaris.Client.Pages
{
    public partial class View_Nurse
    {
        [CascadingParameter]
        private Task<AuthenticationState> authenticationStateTask { get; set; }
        [Inject]
        public INurseService _nurseService { get; set; }
        [Inject]
        public IViewedNurseService _viewedNurseService { get; set; }
        [Inject]
        public IHospitalService _hospitalService { get; set; }
        //public Complain_Nurse modalComplain { get; set; }
        NurseDto nurseDto = new NurseDto();
        NurseDto loggedNurseDto = new NurseDto();
        List<ViewedNurseDto> viewedNurseDtoList = new List<ViewedNurseDto>();
        List<HospitalDto> hospitalList = new List<HospitalDto>();

        public string dataLoaded = "none;";
        public string dataLoading = "block;";
        public string contetntError = "none;";
        public string contetntErrorMsg = "none;";
        public string ModalDisplay = "none;";
        public string ModalClass = "";
        public bool ShowBackdrop = false;
        private int packageMaxViewCount = 0;
        ClaimsPrincipal user;


        protected override async Task OnInitializedAsync()
        {
           user  = (await authenticationStateTask).User;
        }

        //private async void ListLoad()
        //{
        //    hospitalList = await _hospitalService.GetAllAsync();
        //}

        public void Open(long nurseId)
        {
            if (user != null && user.Identity.IsAuthenticated)
            {
                loggedNurseDto = _nurseService.GetByEmail(user.FindFirstValue(ClaimTypes.Email).ToLower());
                if (loggedNurseDto.Email != null)
                {
                    packageMaxViewCount = loggedNurseDto.Package.MaxViewCount;
                }
            }
            else
            {
                loggedNurseDto = new NurseDto();
            }

            hospitalList = _hospitalService.GetAll();

            ModalDisplay = "block;";
            ModalClass = "Show";
            ShowBackdrop = true;
            StateHasChanged();

            if (loggedNurseDto.Email != null && loggedNurseDto.PackageId > 0 && loggedNurseDto.IsActive)
            {

                viewedNurseDtoList = _viewedNurseService.GetByViewerId(loggedNurseDto.Id);
                if (viewedNurseDtoList.Count > 0)
                {
                    if (viewedNurseDtoList.FirstOrDefault(a => a.VieweeId == nurseId) != null)
                    {
                        nurseDto = _nurseService.GetById(nurseId);
                        dataLoaded = "block;";
                        dataLoading = "none;";
                        StateHasChanged();
                    }
                    else if ((viewedNurseDtoList.FirstOrDefault(a => a.VieweeId == nurseId) == null) && (viewedNurseDtoList.Count < packageMaxViewCount))
                    {
                        _viewedNurseService.Create(loggedNurseDto.Id, nurseId);
                        nurseDto = _nurseService.GetById(nurseId);
                        dataLoaded = "block;";
                        dataLoading = "none;";
                        StateHasChanged();
                    }
                    else
                    {
                        contetntError = "block;";
                        contetntErrorMsg = "Package Limit Exceeded";
                    }
                }
                else
                {
                    _viewedNurseService.Create(loggedNurseDto.Id, nurseId);
                    nurseDto = _nurseService.GetById(nurseId);
                    dataLoaded = "block;";
                    dataLoading = "none;";
                    StateHasChanged();
                }
            }
            else if (loggedNurseDto.Email != null && loggedNurseDto.PackageId > 0 && !loggedNurseDto.IsActive)
            {
                contetntError = "block;";
                contetntErrorMsg = "Permission Pending... Please Contact Administrator";
            }
            else
            {
                contetntError = "block;";
                contetntErrorMsg = "Not Authorized";
            }
            StateHasChanged();
        }

        public void Close()
        {
            ModalDisplay = "none;";
            ModalClass = "";
            ShowBackdrop = false;
            dataLoaded = "none;";
            dataLoading = "block;";
            StateHasChanged();
        }

        public List<HospitalDto> GetExpectHospitalByNurseId(long nurseId)
        {
            List<HospitalDto> hospitalList = new List<HospitalDto>();
            if (nurseDto.NurseExpectingHospitalList != null)
            {
                hospitalList = (from hospital in this.hospitalList
                                join joinHospital in this.nurseDto.NurseExpectingHospitalList
                                on hospital.Id equals joinHospital.HospitalId
                                where joinHospital.NurseId == nurseId
                                select hospital).ToList();
            }

            return hospitalList;
        }
    }
}
