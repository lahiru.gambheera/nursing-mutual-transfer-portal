﻿using Dakaris.Contracts.Services;
using Dakaris.Model.Dto.Complain;
using Dakaris.Model.Dto.Nurse;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Dakaris.Client.Pages
{
    public partial class Complain_Nurse
    {
        [CascadingParameter]
        private Task<AuthenticationState> authenticationStateTask { get; set; }
        [Inject]
        public INurseService _nurseService { get; set; }
        [Inject]
        public IComplainService _complainService { get; set; }
        private EditContext EditContext { get; set; }
        protected string OkayDisabled { get; set; } = "disabled";

        NurseDto nurseDto = new NurseDto();
        NurseDto loggedNurseDto = new NurseDto();
        ComplainDto complainDto = new ComplainDto();

        public string dataLoaded = "none;";
        public string dataLoading = "block;";
        public string contetntError = "none;";
        public string contetntErrorMsg = "";
        public string contetntSaveSuccess = "none;";
        public string contetntSuccessMsg = "Success";
        public string ModalDisplay = "none;";
        public string ModalClass = "";
        public bool ShowBackdrop = false;
        ClaimsPrincipal user;

        protected override async Task OnInitializedAsync()
        {
            user = (await authenticationStateTask).User;
            if (user != null && user.Identity.IsAuthenticated)
            {
                loggedNurseDto = _nurseService.GetByEmail(user.FindFirstValue(ClaimTypes.Email).ToLower());
            }
            else
            {
                loggedNurseDto = new NurseDto();
            }
            complainDto.Reportee = nurseDto;
        }

        protected void Submit(ComplainDto complainDto)
        {
            if (complainDto.Description != null)
            {
                complainDto.Id = 0;
                complainDto.ReporterId = loggedNurseDto.Id;
                complainDto.CreatedAt = DateTime.Now;
                complainDto.IsActive = true;

                if (complainDto.ReporterId > 0 && complainDto.ReporteeId > 0)
                {
                    _complainService.Create(complainDto);
                    Close();
                }
            }
        }

        public void Open(long nurseId)
        {
            if (user != null && user.Identity.IsAuthenticated)
            {
                loggedNurseDto = _nurseService.GetByEmail(user.FindFirstValue(ClaimTypes.Email).ToLower());
            }
            else
            {
                loggedNurseDto = new NurseDto();
            }

            ModalDisplay = "block;";
            ModalClass = "Show";
            ShowBackdrop = true;
            StateHasChanged();

            if(nurseId == loggedNurseDto.Id && nurseId != 0)
            {
                contetntSaveSuccess = "none;";
                contetntError = "block;";
                contetntErrorMsg = "Invalid Operation";
                OkayDisabled = "disabled";
                StateHasChanged();
            }
            else if (loggedNurseDto.Email != null && loggedNurseDto.PackageId > 0 && loggedNurseDto.IsActive)
            {
                nurseDto = _nurseService.GetById(nurseId);
                complainDto = new ComplainDto();
                complainDto.ReporteeId = nurseDto.Id;
                complainDto.Reportee = nurseDto;
                dataLoaded = "block;";
                dataLoading = "none;";
                contetntError = "none;";
                OkayDisabled = null;
                StateHasChanged();
            }
            else if (loggedNurseDto.Email != null && loggedNurseDto.PackageId > 0 && !loggedNurseDto.IsActive)
            {
                contetntSaveSuccess = "none;";
                contetntError = "block;";
                contetntErrorMsg = "Permission Pending... Please Contact Administrator";
                OkayDisabled = "disabled";
                StateHasChanged();
            }
            else
            {
                contetntSaveSuccess = "none;";
                contetntError = "block;";
                contetntErrorMsg = "Not Authorized";
                OkayDisabled = "disabled";
                StateHasChanged();
            }

        }

        public void Close()
        {
            ModalDisplay = "none;";
            ModalClass = "";
            ShowBackdrop = false;
            dataLoaded = "none;";
            dataLoading = "block;";
            StateHasChanged();
        }

        protected override bool ShouldRender()
        {
            var renderUI = true;
            return renderUI;
        }

        protected override void OnAfterRender(bool firstRender)
        {
            base.OnAfterRender(firstRender);
            if(EditContext != null)
            {
                SetOkDisabledStatus();
            }
        }

        private void EditContext_OnFieldChanged(object sender, FieldChangedEventArgs e)
        {
            SetOkDisabledStatus();
        }

        private void SetOkDisabledStatus()
        {
            if (EditContext.Validate())
            {
                OkayDisabled = null;
            }
            else
            {
                OkayDisabled = "disabled";
            }
        }
    }
}
