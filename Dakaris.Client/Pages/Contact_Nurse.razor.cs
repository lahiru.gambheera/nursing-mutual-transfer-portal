﻿using Dakaris.Contracts.Services;
using Dakaris.Model.Dto.ContactedNurse;
using Dakaris.Model.Dto.Nurse;
using Dakaris.Model.Dto.ViewedNurse;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Dakaris.Client.Pages
{
    public partial class Contact_Nurse
    {
        [CascadingParameter]
        private Task<AuthenticationState> authenticationStateTask { get; set; }
        [Inject]
        public INurseService _nurseService { get; set; }
        [Inject]
        public IContactedNurseService _contactedNurseService { get; set; }
        [Inject]
        public IViewedNurseService _viewedNurseService { get; set; }
        NurseDto nurseDto = new NurseDto();
        NurseDto loggedNurseDto = new NurseDto();
        List<ContactedNurseDto> contactedNurseDtoList = new List<ContactedNurseDto>();
        List<ViewedNurseDto> viewedNurseDtoList = new List<ViewedNurseDto>();

        public string dataLoaded = "none;";
        public string dataLoading = "block;";
        public string contetntError = "none;";
        public string contetntErrorMsg = "none;";
        public string ModalDisplay = "none;";
        public string ModalClass = "";
        public bool ShowBackdrop = false;
        private int packageMaxViewCount = 0;


        protected override async Task OnInitializedAsync()
        {
            var user = (await authenticationStateTask).User;
            if (user.Identity.IsAuthenticated)
            {
                loggedNurseDto = await _nurseService.GetByEmailAsync(user.FindFirstValue(ClaimTypes.Email).ToLower());
                if (loggedNurseDto.Email != null)
                {
                    packageMaxViewCount = loggedNurseDto.Package.MaxViewCount;
                }
            }
            else
            {
                loggedNurseDto = new NurseDto();
            }
        }

        public async void Open(long nurseId)
        {
            ModalDisplay = "block;";
            ModalClass = "Show";
            ShowBackdrop = true;
            StateHasChanged();

            if (loggedNurseDto.Email != null && loggedNurseDto.PackageId > 0 && loggedNurseDto.IsActive)
            {
                
                viewedNurseDtoList = await Task.Run(() => _viewedNurseService.GetByViewerIdAsync(loggedNurseDto.Id));
                contactedNurseDtoList = await Task.Run(() => _contactedNurseService.GetByContacterIdAsync(loggedNurseDto.Id));
                if ((contactedNurseDtoList.Count > 0) && (contactedNurseDtoList.FirstOrDefault(a => a.ContacteeId == nurseId) != null))
                {
                    nurseDto = await Task.Run(() => _nurseService.GetByIdAsync(nurseId.ToString()));
                    dataLoaded = "block;";
                    dataLoading = "none;";
                    StateHasChanged();
                }
                else if (viewedNurseDtoList.Count > 0)
                {
                    if ((viewedNurseDtoList.FirstOrDefault(a => a.VieweeId == nurseId) != null) || (viewedNurseDtoList.Count < packageMaxViewCount))
                    {
                        await Task.Run(() => _contactedNurseService.CreateWithViewNurseAsync(loggedNurseDto.Id, nurseId));
                        nurseDto = await Task.Run(() => _nurseService.GetByIdAsync(nurseId.ToString()));
                        dataLoaded = "block;";
                        dataLoading = "none;";
                        StateHasChanged();
                    }
                    else
                    {
                        contetntError = "block;";
                        contetntErrorMsg = "Package Limit Exceeded";
                    }
                }
                else
                {
                    await Task.Run(() => _contactedNurseService.CreateWithViewNurseAsync(loggedNurseDto.Id, nurseId));
                    nurseDto = await Task.Run(() => _nurseService.GetByIdAsync(nurseId.ToString()));
                    dataLoaded = "block;";
                    dataLoading = "none;";
                    StateHasChanged();
                }
            }
            else if(loggedNurseDto.Email != null && loggedNurseDto.PackageId > 0 && !loggedNurseDto.IsActive)
            {
                contetntError = "block;";
                contetntErrorMsg = "Permission Pending... Please Contact Administrator";
            }
            else
            {
                contetntError = "block;";
                contetntErrorMsg = "Not Authorized";
            }
            
        }

        public void Close()
        {
            ModalDisplay = "none;";
            ModalClass = "";
            ShowBackdrop = false;
            dataLoaded = "none;";
            dataLoading = "block;";
            StateHasChanged();
        }
    }
}
