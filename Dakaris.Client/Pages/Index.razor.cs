﻿using Dakaris.Contracts.Services;
using Dakaris.Model.Dto.Hospital;
using Dakaris.Model.Dto.Nurse;
using Microsoft.AspNetCore.Components;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.AspNetCore.Components.Authorization;

namespace Dakaris.Client.Pages
{
    public partial class Index
    {
        [Inject]
        public INurseService _nurseService { get; set; }
        [Inject]
        public IHospitalService _hospitalService { get; set; }
        [Inject]
        public INurseExpectingHospitalService _nurseExpectingHospitalService { get; set; }
        [CascadingParameter]
        private Task<AuthenticationState> authenticationStateTask { get; set; }
        public Contact_Nurse modalContact { get; set; }
        public View_Nurse modalView { get; set; }


        List<HospitalDto> hospitalList = new List<HospitalDto>();
        List<NurseDto> nurseSearchlList = new List<NurseDto>();
        NurseDto loggedNurseDto = new NurseDto();
        long expectedHospitalId = 0;
        long currentHospitalId = 0;
        ClaimsPrincipal user;


        protected override async Task OnInitializedAsync()
        {
            ListLoad();
            user = (await authenticationStateTask).User;
            if (user.Identity.IsAuthenticated)
            {
                loggedNurseDto = _nurseService.GetByEmail(user.FindFirstValue(ClaimTypes.Email).ToLower());
            }
            else
            {
                loggedNurseDto = new NurseDto();
                loggedNurseDto.CurrentHospitalId = 0;
            }

            await Search().ConfigureAwait(false);
        }

        private void ListLoad()
        {
            hospitalList = _hospitalService.GetAll();
        }

        public void Reset()
        {
            ListLoad();
            expectedHospitalId = 0;
            currentHospitalId = 0;
            StateHasChanged();
            if (loggedNurseDto == null || loggedNurseDto.Id <= 0)
            {
                loggedNurseDto = new NurseDto();
                loggedNurseDto.CurrentHospitalId = 0;
            }
            Search().ConfigureAwait(false);
        }

        public async Task Search()
        {
            if(currentHospitalId == expectedHospitalId && currentHospitalId != 0)
            {
                nurseSearchlList = new List<NurseDto>();
            }
            else
            {
                nurseSearchlList = await _nurseService.GetAllByCurrentAndExpectedHospitalAsync(currentHospitalId, expectedHospitalId);
            }
            StateHasChanged();
        }
    }
}
