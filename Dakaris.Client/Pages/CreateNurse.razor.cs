﻿using Dakaris.Contracts.Services;
using Dakaris.Model.Dto.Hospital;
using Dakaris.Model.Dto.Nurse;
using Dakaris.Model.Dto.Package;
using Dakaris.Model.Dto.ViewedNurse;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Dakaris.Client.Pages
{
    public partial class  CreateNurse
    {
    [CascadingParameter]
        private Task<AuthenticationState> authenticationStateTask { get; set; }
        [Inject]
        public INurseService _nurseService { get; set; }
        [Inject]
        public IHospitalService _hospitalService { get; set; }
        [Inject]
        public INurseExpectingHospitalService _nurseExpectingHospitalService { get; set; }
        [Inject]
        public IViewedNurseService _viewedNurseService { get; set; }
        [Inject]
        public IPackageService _packageService { get; set; }
        private EditContext EditContext { get; set; }


        protected string OkayDisabled { get; set; } = "disabled";


        NurseDto nurseDto = new NurseDto();
        NurseDto loggedNurseDto = new NurseDto();
        List<HospitalDto> hospitalList = new List<HospitalDto>();
        List<NurseDto> nurseSearchlList = new List<NurseDto>();
        List<NurseExpectingHospitalDto> nurseExpectingHospitalList = new List<NurseExpectingHospitalDto>();

   
        public List<PackageDto> packageList = new List<PackageDto>();
        public PackageDto packageDto = new PackageDto();

        List<ViewedNurseDto> viewedNurseList = new List<ViewedNurseDto>();
        long expectedHospitalId = 0;
        long currentHospitalId = 0;

        int availableViewCount = 0;


        public string dataLoaded = "none;";
        public string dataLoading = "block;";
        public string contetntError = "none;";
        public string contetntErrorMsg = "";
        public string contetntSaveSuccess = "none;";
        public string contetntSuccessMsg = "Success";
        public string ModalDisplay = "none;";
        public string ModalClass = "";
        public bool ShowBackdrop = false;
        ClaimsPrincipal user;

        protected override async Task OnInitializedAsync()
        {

            var user = (await authenticationStateTask).User;
            if (user.Identity.IsAuthenticated)
            {
                loggedNurseDto.Email = user.FindFirstValue(ClaimTypes.Email);
            }
            else
            {
                loggedNurseDto = new NurseDto();
            }
            nurseDto.Email = loggedNurseDto.Email;
            nurseDto.Name = loggedNurseDto.Name;
            nurseDto.CurrentHospitalId = loggedNurseDto.CurrentHospitalId;
            nurseDto.PackageId = loggedNurseDto.PackageId;
            if (loggedNurseDto.PackageId == 0) { nurseDto.PackageId = 4; }
            await ListLoad();
            EditContext = new EditContext(loggedNurseDto);
        }


        private async Task ListLoad()
        {
            nurseExpectingHospitalList = await _nurseExpectingHospitalService.GetAllAsync();
            hospitalList = await _hospitalService.GetAllAsync();
            viewedNurseList = await _viewedNurseService.GetByViewerIdAsync(loggedNurseDto.Id);
            //availableViewCount = loggedNurseDto.Package.MaxViewCount - viewedNurseList.Count;
        }

        public List<HospitalDto> GetExpectHospitalByNurseIdAsync(long nurseId)
        {
            List<HospitalDto> hospitalList = new List<HospitalDto>();
            hospitalList = (from hospital in this.hospitalList
                            join joinHospital in this.nurseExpectingHospitalList
                            on hospital.Id equals joinHospital.HospitalId
                            where joinHospital.NurseId == nurseId
                            select hospital).ToList();

            return hospitalList;
        }


        protected async Task SubmitAsync(NurseDto nurseDto)
        {
            if (nurseDto != null)
            {
                try
                {
                    //if (nurseDto.Id > 0)
                    //{
                    // await _nurseService.CreateAsync(nurseDto);
                    Close();
                    FormReset();
                    NavigationManager.NavigateTo("/");
                    //OnInitialized();


                    //}
                }
                
                catch(Exception ex)
                { }

            }
        }

        protected void OnInitialized()
        {
            NavigationManager.NavigateTo("counter");

        }
        public void FormReset()
        {
            nurseDto = new NurseDto();
            nurseDto.PackageId = loggedNurseDto.PackageId;
            nurseDto.Email = loggedNurseDto.Email;
        }

        protected override bool ShouldRender()
        {
            var renderUI = true;

            return renderUI;
        }

        protected override void OnAfterRender(bool firstRender)
        {
            base.OnAfterRender(firstRender);
            if (EditContext != null)
            {
                SetOkDisabledStatus();
            }
        }

        private void EditContext_OnFieldChanged(object sender, FieldChangedEventArgs e)
        {
            SetOkDisabledStatus();
        }

        private void SetOkDisabledStatus()
        {
            if (EditContext.Validate())
            {
                OkayDisabled = null;
            }
            else
            {
                OkayDisabled = "disabled";
            }
            StateHasChanged();
        }

        public async Task packageSelectionChange(ChangeEventArgs e)
        {
            if (e.Value.ToString() != "default")
            {
                packageDto = await _packageService.GetByNameAsync(e.Value.ToString());
                if (packageDto == null)
                {
                    packageDto = new PackageDto();
                    packageDto.Name = e.Value.ToString();
                    packageDto.Id = 0;
                }
                //FieldDisabled = null;
            }
            else
            {
                //FormReset();
            }
        }





        public void Close()
        {
            ModalDisplay = "none;";
            ModalClass = "";
            ShowBackdrop = false;
            dataLoaded = "none;";
            dataLoading = "block;";
            StateHasChanged();
        }

        //public void Open(long nurseId)
        //{
        //    if (user != null && user.Identity.IsAuthenticated)
        //    {
        //        loggedNurseDto = _nurseService.GetByEmail(user.FindFirstValue(ClaimTypes.Email).ToLower());
        //    }
        //    else
        //    {
        //        loggedNurseDto = new NurseDto();
        //    }

        //    ModalDisplay = "block;";
        //    ModalClass = "Show";
        //    ShowBackdrop = true;
        //    StateHasChanged();

        //    if (nurseId == loggedNurseDto.Id && nurseId != 0)
        //    {
        //        contetntSaveSuccess = "none;";
        //        contetntError = "block;";
        //        contetntErrorMsg = "Invalid Operation";
        //        OkayDisabled = "disabled";
        //        StateHasChanged();
        //    }
        //    else if (loggedNurseDto.Email != null && loggedNurseDto.PackageId > 0 && loggedNurseDto.IsActive)
        //    {
        //        nurseDto = _nurseService.GetById(nurseId);
        //        //complainDto = new ComplainDto();
        //        //complainDto.ReporteeId = nurseDto.Id;
        //        //complainDto.Reportee = nurseDto;
        //        dataLoaded = "block;";
        //        dataLoading = "none;";
        //        contetntError = "none;";
        //        OkayDisabled = null;
        //        StateHasChanged();
        //    }
        //    else if (loggedNurseDto.Email != null && loggedNurseDto.PackageId > 0 && !loggedNurseDto.IsActive)
        //    {
        //        contetntSaveSuccess = "none;";
        //        contetntError = "block;";
        //        contetntErrorMsg = "Permission Pending... Please Contact Administrator";
        //        OkayDisabled = "disabled";
        //        StateHasChanged();
        //    }
        //    else
        //    {
        //        contetntSaveSuccess = "none;";
        //        contetntError = "block;";
        //        contetntErrorMsg = "Not Authorized";
        //        OkayDisabled = "disabled";
        //        StateHasChanged();
        //    }

        //}


    }
}
