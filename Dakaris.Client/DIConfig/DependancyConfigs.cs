﻿using Dakaris.Contracts.Services;
using Dakaris.Service.Core;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dakaris.Client.DIConfig
{
    public class DependancyConfigs
    {
        public static void Inject(IServiceCollection services)
        {
            services.AddScoped<INurseService, NurseService>();
            services.AddScoped<IComplainService, ComplainService>();
            services.AddScoped<IPackageService, PackageService>();
            services.AddScoped<IHospitalService, HospitalService>();
            services.AddScoped<INurseExpectingHospitalService, NurseExpectingHospitalService>();
            services.AddScoped<IAdminService, AdminService>();
            services.AddScoped<IContactedNurseService, ContactedNurseService>();
            services.AddScoped<IViewedNurseService, ViewedNurseService>();
        }
    }
}
