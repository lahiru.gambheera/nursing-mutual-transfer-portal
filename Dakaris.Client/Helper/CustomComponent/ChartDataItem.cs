﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace Dakaris.Client.Helper.CustomComponent
{
    public class ChartDataItem
    {
        public string LabelName { get; set; }
        public double Value { get; set; }
        public Color Color { get; set; }
    }
}
