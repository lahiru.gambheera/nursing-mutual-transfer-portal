using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Dakaris.Contracts.Services;
using Dakaris.Model.Dto.Nurse;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;

namespace Dakaris.Client.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class ExternalLoginModel : PageModel
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IEmailSender _emailSender;
        private readonly ILogger<ExternalLoginModel> _logger;
        [Inject]
        public INurseService _nurseService { get; set; }
        [Inject]
        public IAdminService _adminService { get; set; }

        public ExternalLoginModel(
            SignInManager<IdentityUser> signInManager,
            UserManager<IdentityUser> userManager,
            ILogger<ExternalLoginModel> logger,
            IEmailSender emailSender,
            INurseService nurseService,
            IAdminService adminService)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _logger = logger;
            _emailSender = emailSender;
            _nurseService = nurseService;
            _adminService = adminService;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string LoginProvider { get; set; }

        public string ReturnUrl { get; set; }

        [TempData]
        public string ErrorMessage { get; set; }

        public class InputModel
        {
            [Required]
            [EmailAddress]
            public string Email { get; set; }
        }

        public IActionResult OnGetAsync()
        {
            string returnUrl = null;
            //return RedirectToPage("./Login");
            var redirectUrl = Url.Page("./ExternalLogin", pageHandler: "Callback", values: new { returnUrl });
            var properties = _signInManager.ConfigureExternalAuthenticationProperties("Google", redirectUrl);
            return new ChallengeResult("Google", properties);
        }

        public IActionResult OnPost(string provider, string returnUrl = null)
        {
            // Request a redirect to the external login provider.
            var redirectUrl = Url.Page("./ExternalLogin", pageHandler: "Callback", values: new { returnUrl });
            var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
            return new ChallengeResult(provider, properties);
        }

        //[HttpPost]
        //public IActionResult OnPost()
        //{
        //    string returnUrl = "/";
        //    string provider = "Google";
        //    // Request a redirect to the external login provider.
        //    var redirectUrl = Url.Page("./ExternalLogin", pageHandler: "Callback", values: new { returnUrl });
        //    var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
        //    return new ChallengeResult(provider, properties);
        //}

        //[HttpGet]
        //public IActionResult Get(string provider, string returnUrl = null)
        //{
        //    // Request a redirect to the external login provider.
        //    var redirectUrl = Url.Page("./ExternalLogin", pageHandler: "Callback", values: new { returnUrl });
        //    var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
        //    return new ChallengeResult(provider, properties);
        //}


        public async Task<IActionResult> OnGetCallbackAsync(string returnUrl = null, string remoteError = null)
        {
            NurseDto loggedNurse = new NurseDto();
            AdminDto admin = new AdminDto();
            string roleType = "User";
            returnUrl = returnUrl ?? Url.Content("~/");
            if (remoteError != null)
            {
                ErrorMessage = $"Error from external provider: {remoteError}";
                return RedirectToPage("./Login", new { ReturnUrl = returnUrl });
            }
            var info = await _signInManager.GetExternalLoginInfoAsync();
            if (info == null)
            {
                ErrorMessage = "Error loading external login information.";
                return RedirectToPage("./Login", new { ReturnUrl = returnUrl });
            }
            // Search whether user is exist in the system
            else if (info.Principal.HasClaim(c => c.Type == ClaimTypes.Email))
            {
                string loginEmail = info.Principal.FindFirstValue(ClaimTypes.Email).ToLower();
                loggedNurse = await _nurseService.GetByEmailAsync(loginEmail);
                admin = await _adminService.GetByEmailAsync(loginEmail);
                roleType = "User";

                if (admin != null && admin.IsActive)
                    roleType = "Administrator";

                ClaimsIdentity roleIdentity = new ClaimsIdentity();
                roleIdentity.AddClaim(new Claim(ClaimTypes.Role, roleType));
                info.Principal.AddIdentity(roleIdentity);

                //var GoogleUser = info.Principal.Identities.FirstOrDefault();
                //if (GoogleUser.IsAuthenticated)
                //{
                //    var authProperties = new AuthenticationProperties
                //    {
                //        IsPersistent = true,
                //        RedirectUri = this.Request.Host.Value
                //    };
                //    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(GoogleUser), authProperties);
                //}
            }
            //string loginEmail = info.Principal.Claims.Where(a=>a.Type.Contains("emailaddress")).FirstOrDefault().Value.ToLower();
            // Sign in the user with this external login provider if the user already has a login.
            //await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false, bypassTwoFactor: true);

            
            //If user not in database send to the registration page
            if (loggedNurse.Email == null && roleType == "User")
            {
                _logger.LogInformation("{Name} logged in with {LoginProvider} provider.", info.Principal.Identity.Name, info.LoginProvider);
                try
                {
                    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, info.Principal, info.AuthenticationProperties);
                }
                catch (Exception ex)
                {

                    string error = ex.Message;

                }
                return Redirect("./createNurse");
            }
            if (loggedNurse.IsSuspended)
            {
                return RedirectToPage("./Lockout");
            }
            else
            {
                // If the user does not have an account, then ask the user to create an account.
                ReturnUrl = returnUrl;
                LoginProvider = info.LoginProvider;
                if (info.Principal.HasClaim(c => c.Type == ClaimTypes.Email))
                {
                    Input = new InputModel
                    {
                        Email = info.Principal.FindFirstValue(ClaimTypes.Email)
                    };//HttpContext.User.

                    //var claims = new List<Claim>
                    //{
                    //    new Claim(ClaimTypes.Email, info.Principal.FindFirstValue(ClaimTypes.Email)),
                    //    new Claim(ClaimTypes.Name, info.Principal.FindFirstValue(ClaimTypes.Name)),
                    //    new Claim(ClaimTypes.Role, "googleUser"),
                    //};

                    //var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                    //var authProperties = new AuthenticationProperties
                    //{
                    //    IsPersistent = true,
                    //    RedirectUri = this.Request.Host.Value
                    //};

                    try
                    {
                        await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, info.Principal, info.AuthenticationProperties);
                    }
                    catch (Exception ex)
                    {

                        string error = ex.Message;

                    }

                    if (info.Principal.FindFirstValue(ClaimTypes.Role) == "Administrator")
                    {
                        Response.Redirect(string.Format("/admin/dashboard"));
                    }
                    else
                    {
                        Response.Redirect(string.Format("/"));
                    }
                    //Response.Redirect(string.Format("/Home"));
                    
                }
                return Page();
            }
        }

        //public async Task<IActionResult> OnPostConfirmationAsync(string returnUrl = null)
        //{
        //    returnUrl = returnUrl ?? Url.Content("~/");
        //    // Get the information about the user from the external login provider
        //    var info = await _signInManager.GetExternalLoginInfoAsync();
        //    if (info == null)
        //    {
        //        ErrorMessage = "Error loading external login information during confirmation.";
        //        return RedirectToPage("./Login", new { ReturnUrl = returnUrl });
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        var user = new IdentityUser { UserName = Input.Email, Email = Input.Email };
        //        var result = await _userManager.CreateAsync(user);
        //        if (result.Succeeded)
        //        {
        //            result = await _userManager.AddLoginAsync(user, info);
        //            if (result.Succeeded)
        //            {
        //                _logger.LogInformation("User created an account using {Name} provider.", info.LoginProvider);

        //                // If account confirmation is required, we need to show the link if we don't have a real email sender
        //                if (_userManager.Options.SignIn.RequireConfirmedAccount)
        //                {
        //                    return RedirectToPage("./RegisterConfirmation", new { Email = Input.Email });
        //                }

        //                await _signInManager.SignInAsync(user, isPersistent: false);
        //                var userId = await _userManager.GetUserIdAsync(user);
        //                var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
        //                code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
        //                var callbackUrl = Url.Page(
        //                    "/Account/ConfirmEmail",
        //                    pageHandler: null,
        //                    values: new { area = "Identity", userId = userId, code = code },
        //                    protocol: Request.Scheme);

        //                await _emailSender.SendEmailAsync(Input.Email, "Confirm your email",
        //                    $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

        //                return LocalRedirect(returnUrl);
        //            }
        //        }
        //        foreach (var error in result.Errors)
        //        {
        //            ModelState.AddModelError(string.Empty, error.Description);
        //        }
        //    }

        //    LoginProvider = info.LoginProvider;
        //    ReturnUrl = returnUrl;
        //    return Page();
        //}
    }
}
