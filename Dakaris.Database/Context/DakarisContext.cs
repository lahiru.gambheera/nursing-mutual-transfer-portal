﻿using Dakaris.Model.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dakaris.Database.Context
{
    public class DakarisContext : DbContext
    {
        public DbSet<Hospital> Hospitals { get; set; }
        public DbSet<Nurse> Nurses { get; set; }
        public DbSet<Package> Packages { get; set; }
        public DbSet<ContactedNurse> ContactedNurses { get; set; }
        public DbSet<NurseExpectingHospital> NurseExpectingHospitals { get; set; }
        public DbSet<ViewedNurse> ViewedNurses { get; set; }
        public DbSet<Complain> Complains { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<PackageRequest> PackageRequests { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<GirlyName> GirlyNames { get; set; }


        public DakarisContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region PRIMARY KEYS

            modelBuilder.Entity<Hospital>().HasKey(p => new { p.Id });
            modelBuilder.Entity<Complain>().HasKey(p => new { p.Id });
            modelBuilder.Entity<Nurse>().HasKey(p => new { p.Id });
            modelBuilder.Entity<Package>().HasKey(p => new { p.Id });
            modelBuilder.Entity<ContactedNurse>().HasKey(p => new { p.ContacterId, p.ContacteeId });
            modelBuilder.Entity<NurseExpectingHospital>().HasKey(p => new { p.NurseId, p.hospitalId });
            modelBuilder.Entity<NurseExpectingHospital>().Property(n => n.Id).UseSqlServerIdentityColumn();
            modelBuilder.Entity<ViewedNurse>().HasKey(p => new { p.ViewerId, p.VieweeId });
            modelBuilder.Entity<Admin>().HasKey(p => new { p.Id });
            modelBuilder.Entity<PackageRequest>().HasKey(p => new { p.Id });
            modelBuilder.Entity<Notification>().HasKey(p => new { p.Id });
            modelBuilder.Entity<GirlyName>().HasKey(p => new { p.Id });

            #endregion

            #region FORIEGN KEYS

            modelBuilder.Entity<Nurse>().HasOne(dv => dv.Package).WithMany(u => u.Nurses).HasForeignKey(v => v.PackageId);
            modelBuilder.Entity<Nurse>().HasOne(dv => dv.CurrentHospital).WithMany(u => u.Nurses).HasForeignKey(v => v.CurrentHospitalId).OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<Notification>().HasOne(dv => dv.Nurse).WithMany(u => u.NotificationList).HasForeignKey(v => v.NurseId);

            modelBuilder.Entity<ContactedNurse>().HasOne(dv => dv.Contacter).WithMany(u => u.ContacterList).HasForeignKey(v => v.ContacterId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<ContactedNurse>().HasOne(dv => dv.Contactee).WithMany(u => u.ContacteeList).HasForeignKey(v => v.ContacteeId);

            modelBuilder.Entity<NurseExpectingHospital>().HasOne(dv => dv.Hospital).WithMany(u => u.NurseExpectingHospitalList).HasForeignKey(v => v.hospitalId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<NurseExpectingHospital>().HasOne(dv => dv.Nurse).WithMany(u => u.NurseExpectingHospitalList).HasForeignKey(v => v.NurseId);

            modelBuilder.Entity<ViewedNurse>().HasOne(dv => dv.Viewer).WithMany(u => u.ViewerList).HasForeignKey(v => v.ViewerId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<ViewedNurse>().HasOne(dv => dv.Viewee).WithMany(u => u.VieweeList).HasForeignKey(v => v.VieweeId);

            modelBuilder.Entity<Complain>().HasOne(dv => dv.Reporter).WithMany(u => u.ReporterList).HasForeignKey(v => v.ReporterId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Complain>().HasOne(dv => dv.Reportee).WithMany(u => u.ReporteeList).HasForeignKey(v => v.ReporteeId);

            modelBuilder.Entity<PackageRequest>().HasOne(dv => dv.Nurse).WithMany(u => u.PackageRequestList).HasForeignKey(v => v.NurseId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<PackageRequest>().HasOne(dv => dv.Package).WithMany(u => u.PackageRequestList).HasForeignKey(v => v.PackageId);

            #endregion
        }
    }
}
