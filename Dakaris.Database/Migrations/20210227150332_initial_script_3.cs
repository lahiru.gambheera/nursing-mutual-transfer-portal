﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Dakaris.Database.Migrations
{
    public partial class initial_script_3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "GoogleId",
                table: "Nurses",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GoogleId",
                table: "Nurses");
        }
    }
}
