﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Dakaris.Database.Migrations
{
    public partial class initial_script : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Admins",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Email = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Admins", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GirlyNames",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    AvatarUrl = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GirlyNames", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Hospitals",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    District = table.Column<int>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Hospitals", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Packages",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Price = table.Column<decimal>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    MaxViewCount = table.Column<int>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Packages", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Nurses",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Nic = table.Column<string>(nullable: true),
                    MobileNumber = table.Column<string>(nullable: true),
                    ExtraMobileNo = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    MyPromoCode = table.Column<string>(nullable: true),
                    RegisteredPromoCode = table.Column<string>(nullable: true),
                    HasPaidForPromo = table.Column<bool>(nullable: false),
                    CurrentHospitalId = table.Column<long>(nullable: true),
                    PackageId = table.Column<long>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    IsSuspended = table.Column<bool>(nullable: false),
                    AvailableViewCredits = table.Column<int>(nullable: false),
                    ViewCount = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Nurses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Nurses_Hospitals_CurrentHospitalId",
                        column: x => x.CurrentHospitalId,
                        principalTable: "Hospitals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_Nurses_Packages_PackageId",
                        column: x => x.PackageId,
                        principalTable: "Packages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Complains",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ReporterId = table.Column<long>(nullable: false),
                    ReporteeId = table.Column<long>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Complains", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Complains_Nurses_ReporteeId",
                        column: x => x.ReporteeId,
                        principalTable: "Nurses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Complains_Nurses_ReporterId",
                        column: x => x.ReporterId,
                        principalTable: "Nurses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContactedNurses",
                columns: table => new
                {
                    ContacterId = table.Column<long>(nullable: false),
                    ContacteeId = table.Column<long>(nullable: false),
                    Id = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactedNurses", x => new { x.ContacterId, x.ContacteeId });
                    table.ForeignKey(
                        name: "FK_ContactedNurses_Nurses_ContacteeId",
                        column: x => x.ContacteeId,
                        principalTable: "Nurses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContactedNurses_Nurses_ContacterId",
                        column: x => x.ContacterId,
                        principalTable: "Nurses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Notifications",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NurseId = table.Column<long>(nullable: false),
                    Time = table.Column<DateTime>(nullable: false),
                    Message = table.Column<string>(nullable: true),
                    HiddenMessage = table.Column<string>(nullable: true),
                    IsRead = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsActionButtonAvailable = table.Column<bool>(nullable: false),
                    ActionButtonText = table.Column<string>(nullable: true),
                    ActionButtonLink = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notifications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Notifications_Nurses_NurseId",
                        column: x => x.NurseId,
                        principalTable: "Nurses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "NurseExpectingHospitals",
                columns: table => new
                {
                    NurseId = table.Column<long>(nullable: false),
                    hospitalId = table.Column<long>(nullable: false),
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NurseExpectingHospitals", x => new { x.NurseId, x.hospitalId });
                    table.ForeignKey(
                        name: "FK_NurseExpectingHospitals_Nurses_NurseId",
                        column: x => x.NurseId,
                        principalTable: "Nurses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_NurseExpectingHospitals_Hospitals_hospitalId",
                        column: x => x.hospitalId,
                        principalTable: "Hospitals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PackageRequests",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NurseId = table.Column<long>(nullable: false),
                    PackageId = table.Column<long>(nullable: false),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PackageRequests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PackageRequests_Nurses_NurseId",
                        column: x => x.NurseId,
                        principalTable: "Nurses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PackageRequests_Packages_PackageId",
                        column: x => x.PackageId,
                        principalTable: "Packages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ViewedNurses",
                columns: table => new
                {
                    ViewerId = table.Column<long>(nullable: false),
                    VieweeId = table.Column<long>(nullable: false),
                    Id = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ViewedNurses", x => new { x.ViewerId, x.VieweeId });
                    table.ForeignKey(
                        name: "FK_ViewedNurses_Nurses_VieweeId",
                        column: x => x.VieweeId,
                        principalTable: "Nurses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ViewedNurses_Nurses_ViewerId",
                        column: x => x.ViewerId,
                        principalTable: "Nurses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Complains_ReporteeId",
                table: "Complains",
                column: "ReporteeId");

            migrationBuilder.CreateIndex(
                name: "IX_Complains_ReporterId",
                table: "Complains",
                column: "ReporterId");

            migrationBuilder.CreateIndex(
                name: "IX_ContactedNurses_ContacteeId",
                table: "ContactedNurses",
                column: "ContacteeId");

            migrationBuilder.CreateIndex(
                name: "IX_Notifications_NurseId",
                table: "Notifications",
                column: "NurseId");

            migrationBuilder.CreateIndex(
                name: "IX_NurseExpectingHospitals_hospitalId",
                table: "NurseExpectingHospitals",
                column: "hospitalId");

            migrationBuilder.CreateIndex(
                name: "IX_Nurses_CurrentHospitalId",
                table: "Nurses",
                column: "CurrentHospitalId");

            migrationBuilder.CreateIndex(
                name: "IX_Nurses_PackageId",
                table: "Nurses",
                column: "PackageId");

            migrationBuilder.CreateIndex(
                name: "IX_PackageRequests_NurseId",
                table: "PackageRequests",
                column: "NurseId");

            migrationBuilder.CreateIndex(
                name: "IX_PackageRequests_PackageId",
                table: "PackageRequests",
                column: "PackageId");

            migrationBuilder.CreateIndex(
                name: "IX_ViewedNurses_VieweeId",
                table: "ViewedNurses",
                column: "VieweeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Admins");

            migrationBuilder.DropTable(
                name: "Complains");

            migrationBuilder.DropTable(
                name: "ContactedNurses");

            migrationBuilder.DropTable(
                name: "GirlyNames");

            migrationBuilder.DropTable(
                name: "Notifications");

            migrationBuilder.DropTable(
                name: "NurseExpectingHospitals");

            migrationBuilder.DropTable(
                name: "PackageRequests");

            migrationBuilder.DropTable(
                name: "ViewedNurses");

            migrationBuilder.DropTable(
                name: "Nurses");

            migrationBuilder.DropTable(
                name: "Hospitals");

            migrationBuilder.DropTable(
                name: "Packages");
        }
    }
}
